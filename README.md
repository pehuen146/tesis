﻿# Resurrected

![picture](logo.png)

The story about Roy, a CIA ex-agent, living in an apocaliptic world, where the most of the people have become zombies.
He must find the cure of the virus causing this, to solve this caos.

This is a game (demo/protoype/vertical slice) developed by students from [Image Campus](https://www.imagecampus.edu.ar)


![Alt text](logo-image-campus.png)


## Credits
(in alphabetical order)

- **Nicolas Emanuel Susterman** - *Programming* - [LinkedIn](https://www.linkedin.com/in/nicolas-susterman-25793b180/)
- **Pehuen Testa** - *Programming* - [LinkedIn](https://www.linkedin.com/in/pehuentesta/)

This game was also possible thanks to the support of these professors:

- **Roy Magariños**
- **Andrés Vergez**


## Links

Download it from itch.io: https://pehuentesta.itch.io/resurrected