﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CarInput))]
[RequireComponent(typeof(Rigidbody))]
public class CarController : MonoBehaviour
{
    [SerializeField] List<WheelCollider> throttleWheels;
    [SerializeField] List<WheelCollider> steeringWheels;
    [SerializeField] float strenghtCoefficient = 20000;
    [SerializeField] float maxTurn = 20;
    [SerializeField] Transform centerOfMass;
    [SerializeField] float brakeStrenght;

    Rigidbody rb;
    CarInput ci;

    void Start()
    {
        ci = GetComponent<CarInput>();
        rb = GetComponent<Rigidbody>();

        if (centerOfMass)
            rb.centerOfMass = centerOfMass.localPosition;

    }

    void FixedUpdate()
    {
        foreach (WheelCollider wheel in throttleWheels)
        {
            if (!ci.GetIsDestroyed())
            {
                if (ci.GetBrake())
                {
                    wheel.motorTorque = 0;
                    wheel.brakeTorque = brakeStrenght * Time.deltaTime;
                }
                else
                {
                    wheel.motorTorque = strenghtCoefficient * Time.deltaTime * ci.GetThrottle();
                    wheel.brakeTorque = 0;
                }
            }
            else
            {
                wheel.motorTorque = 0;
                wheel.brakeTorque = brakeStrenght * Time.deltaTime;
            }
        }

        foreach (WheelCollider wheel in steeringWheels)
        {
            wheel.steerAngle = maxTurn * ci.GetSteer();
        }

    }

    public float GetVelocity()
    {
        /*if (ci.GetThrottle() > 0)
            return rb.velocity.magnitude;
        else
            return -rb.velocity.magnitude;*/

        /*if (ci.GetPlayer())
            return rb.velocity.magnitude * (transform.InverseTransformDirection(rb.velocity).z >= 0 ? 1 : -1) / (2 * Mathf.PI * 0.33f);
        else
            return 0;*/

        return Mathf.Clamp(Mathf.Round(transform.InverseTransformVector(rb.velocity).z) * 3.6f, -1000, 1000);
    }

}
