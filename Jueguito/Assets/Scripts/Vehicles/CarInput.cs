﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarInput : MonoBehaviour
{
    [SerializeField] float maxResistance;
    [SerializeField] ResistanceBarScript resistanceBar;
    [SerializeField] Animator[] frontWheels;

    GameObject player;
    GameObject cameraTransform;
    GetSpawnTransform playerSpawnPoint;
    VehiclesSound vehiclesSound;
    float resistance;
    bool isMoving = false;
    bool isDestroyed = false;
    CarController cc;

    float throttle;
    float steer;
    bool brake;

    private void Start()
    {
        cc = GetComponent<CarController>();
        resistance = maxResistance;
        playerSpawnPoint = GetComponentInChildren<GetSpawnTransform>();
        vehiclesSound = GetComponent<VehiclesSound>();
    }

    private void FixedUpdate()
    {
        if (player && vehiclesSound.GetHasStarted())
        {
            if (resistance > 0)
            {
                throttle = Input.GetAxis("Vertical");
                steer = Input.GetAxis("Horizontal");
            }
            else
                isDestroyed = true;

            vehiclesSound.PlayAudioEngine(cc.GetVelocity(), 50);
            resistanceBar.UpdateResistanceBar(resistance, maxResistance);
        }
        else
        {
            throttle = 0;
            steer = 0;
        }

        foreach (var wheel in frontWheels)
            wheel.SetFloat("Direction", steer, 1f, Time.deltaTime * 20f);

        brake = Input.GetKey(KeyCode.Space);

        if (cc.GetVelocity() != 0)
            isMoving = true;
        else
            isMoving = false;
        
        if (player && Input.GetKeyDown(KeyCode.E))
        {
            vehiclesSound.StopAudio();
            vehiclesSound.SetHasStarted(false);
            player.transform.position = playerSpawnPoint.GetSpawnPoint();
            player.SetActive(true);
            player.GetComponent<PlayerMovement>().ResistanceRefill();
            cameraTransform.transform.parent = player.transform;
            cameraTransform.transform.position = player.transform.position;
            resistanceBar.gameObject.SetActive(false);
            player = null;
        }
    }

    public float GetThrottle()
    {
        return throttle;
    }

    public float GetSteer()
    {
        return steer;
    }

    public void SetPlayer(GameObject _player)
    {
        player = _player;
        resistanceBar.gameObject.SetActive(true);
        vehiclesSound.PlayAudio();
    }

    public GameObject GetPlayer()
    {
        return player;
    }

    public void SetCameraTransform(GameObject _cameraTransform)
    {
        cameraTransform = _cameraTransform;
    }

    public void ResistanceSubstraction(float crashDamage)
    {
        resistance -= crashDamage;
    }

    public void RepairVehicle(float _amount)
    {
        resistance += _amount;
        if (resistance > maxResistance)
            resistance = maxResistance;
        isDestroyed = false;
    }

    public bool GetIsMoving()
    {
        return isMoving;
    }

    public bool GetBrake()
    {
        return brake;
    }

    public bool GetIsDestroyed()
    {
        return isDestroyed;
    }

}
