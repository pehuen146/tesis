﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfCarSpawnPointCheck : MonoBehaviour
{
    //GetSpawnTransform spawnTransform;
    bool isAble = true;

    /*private void Start()
    {
        spawnTransform = GetComponentInParent<GetSpawnTransform>();
    }*/

    private void OnTriggerEnter(Collider other)
    {
        isAble = false;
    }

    private void OnTriggerStay(Collider other)
    {
        isAble = false;
    }

    private void OnTriggerExit(Collider other)
    {
        isAble = true;
    }

    public bool GetIsAble()
    {
        return isAble;
    }

}
