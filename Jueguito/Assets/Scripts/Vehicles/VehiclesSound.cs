﻿using System.Collections;
using UnityEngine;

public class VehiclesSound : MonoBehaviour
{
    [SerializeField] AudioClip[] audioClips;

    AudioSource audioSource;
    bool hasStarted = false;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = audioClips[2];
    }

    public void PlayAudio()
    {
        StartCoroutine(playEngineSound());
        audioSource.loop = false;
    }

    public void PlayAudioCrash()
    {
        audioSource.PlayOneShot(audioClips[1]);
    }

    public void PlayAudioEngine(float currentVelocity, float maxVelocity)
    {
        float currentPitch;
        currentPitch = currentVelocity / maxVelocity;

        if (currentPitch < 0)
            currentPitch *= -1;

        currentPitch += 1;
        audioSource.pitch = currentPitch;
        audioSource.loop = true;
    }

    public void StopAudio()
    {
        audioSource.Stop();
    }

    IEnumerator playEngineSound()
    {
        audioSource.clip = audioClips[2];
        audioSource.Play();
        yield return new WaitForSeconds(audioSource.clip.length);
        audioSource.clip = audioClips[0];
        audioSource.Play();
        hasStarted = true;
    }

    public bool GetHasStarted()
    {
        return hasStarted;
    }

    public void SetHasStarted(bool _hasStarted)
    {
        hasStarted = _hasStarted;
    }

}