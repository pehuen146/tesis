﻿using UnityEngine;
using System;

[Serializable]
public enum DriveType
{
    RearWheelDrive,
    FrontWheelDrive,
    AllWheelDrive
}

public class WheelDrive : MonoBehaviour
{
    [Tooltip("Maximum steering angle of the wheels")]
    public float maxAngle = 30f;
    [Tooltip("Maximum torque applied to the driving wheels")]
    public float maxTorque = 300f;
    [Tooltip("Maximum brake torque applied to the driving wheels")]
    public float brakeTorque = 30000f;

    [Tooltip("The vehicle's speed when the physics engine can use different amount of sub-steps (in m/s).")]
    public float criticalSpeed = 5f;
    [Tooltip("Simulation sub-steps when the speed is above critical.")]
    public int stepsBelow = 5;
    [Tooltip("Simulation sub-steps when the speed is below critical.")]
    public int stepsAbove = 1;

    [Tooltip("The vehicle's drive type: rear-wheels drive, front-wheels drive or all-wheels drive.")]
    public DriveType driveType;

    [SerializeField] float maxResistance;
    [SerializeField] ResistanceBarScript resistanceBar;
    [SerializeField] Animator[] frontWheels;

    WheelCollider[] m_Wheels;
    GameObject player;
    GameObject cameraTransform;
    GetSpawnTransform playerSpawnPoint;
    Rigidbody m_Rigidbody;
    VehiclesSound vehiclesSound;
    float angle = 0;
    float torque = 0;
    float handBrake;
    float resistance;
    bool isMoving = false;

    void Start()
    {
        m_Wheels = GetComponentsInChildren<WheelCollider>();
        resistance = maxResistance;
        playerSpawnPoint = GetComponentInChildren<GetSpawnTransform>();
        m_Rigidbody = GetComponent<Rigidbody>();
        vehiclesSound = GetComponent<VehiclesSound>();
    }

    void FixedUpdate()
    {
        if (player)
        {
            if (resistance > 0)
            {
                m_Wheels[0].ConfigureVehicleSubsteps(criticalSpeed, stepsBelow, stepsAbove);

                float horizontal = Input.GetAxis("Horizontal");
                torque = maxTorque * Input.GetAxis("Vertical");
                angle = maxAngle * horizontal;


                foreach (var wheel in frontWheels)
                    wheel.SetFloat("Direction", horizontal, 1f, Time.deltaTime * 20f);

                handBrake = Input.GetKey(KeyCode.Space) ? brakeTorque : 0;

                if (torque != 0)
                    isMoving = true;
                else
                    isMoving = false;

                foreach (WheelCollider wheel in m_Wheels)
                {
                    if (wheel.transform.localPosition.z > 0)
                        wheel.steerAngle = angle;

                    if (wheel.transform.localPosition.z < 0)
                        wheel.brakeTorque = handBrake;

                    if (wheel.transform.localPosition.z < 0 && driveType != DriveType.FrontWheelDrive)
                        wheel.motorTorque = torque;

                    if (wheel.transform.localPosition.z >= 0 && driveType != DriveType.RearWheelDrive)
                        wheel.motorTorque = torque;
                }
            }
            vehiclesSound.PlayAudioEngine(GetVelocity(), criticalSpeed);
            resistanceBar.UpdateResistanceBar(resistance, maxResistance);
        }

        if (player && Input.GetKeyDown(KeyCode.E))
        {
            isMoving = false;
            vehiclesSound.StopAudio();
            player.transform.position = playerSpawnPoint.GetSpawnPoint();
            player.SetActive(true);
            cameraTransform.transform.parent = player.transform;
            cameraTransform.transform.position = player.transform.position;
            resistanceBar.gameObject.SetActive(false);
            player = null;
        }
    }

    public void SetPlayer(GameObject _player)
    {
        player = _player;
        resistanceBar.gameObject.SetActive(true);
        //vehiclesSound.PlayAudio();
    }

    public void SetCameraTransform(GameObject _cameraTransform)
    {
        cameraTransform = _cameraTransform;
    }

    public void ResistanceSubstraction(float crashDamage)
    {
        resistance -= crashDamage;
    }

    public float GetVelocity()
    {
        if (torque > 0)
            return m_Rigidbody.velocity.magnitude;
        else
            return -m_Rigidbody.velocity.magnitude;
    }

    public bool GetIsMoving()
    {
        return isMoving;
    }

}
