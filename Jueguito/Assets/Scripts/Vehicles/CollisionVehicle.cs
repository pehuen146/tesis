﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionVehicle : MonoBehaviour
{
    [SerializeField] float damage;
    [SerializeField] float crashDamage;

    CarInput ci;
    CarController cc;
    VehiclesSound vehiclesSound;
    Timer timer;

    private void Start()
    {
        ci = GetComponent<CarInput>();
        cc = GetComponent<CarController>();
        vehiclesSound = GetComponent<VehiclesSound>();
        timer = new Timer();
        timer.SetTime(1.5f, false);
    }

    private void FixedUpdate()
    {
        timer.Update();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (ci && ci.GetIsMoving())
        {
            if (collision.gameObject.CompareTag("Environment"))
            {
                if (timer.TimeUp())
                {
                    ci.ResistanceSubstraction(crashDamage);
                    vehiclesSound.PlayAudioCrash();
                    timer.Reset();
                }

            }
            else
            {
                if (collision.gameObject.CompareTag("Enemy"))
                {
                    EnemyHealth enemyHealth = collision.gameObject.GetComponent<EnemyHealth>();
                    enemyHealth.TakeDamage(damage);
                }

                if (collision.gameObject.CompareTag("Car"))
                {
                    CarInput otherCar = collision.gameObject.GetComponent<CarInput>();
                    otherCar.ResistanceSubstraction(crashDamage);
                }

                ci.ResistanceSubstraction(crashDamage);
                vehiclesSound.PlayAudioCrash();
            }
            
        }
    }

}
