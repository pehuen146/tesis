﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetSpawnTransform : MonoBehaviour {

    public enum SpawnPoints { first, second, third}

    [SerializeField] Transform[] transforms;
    [SerializeField] OutOfCarSpawnPointCheck[] outOfCarSpawns;

    Vector3 spawnPosition;

    private void Start()
    {
        spawnPosition = transforms[0].position;
    }

    public Vector3 GetSpawnPoint()
    {
        return spawnPosition;
    }

    private void Update()
    {
        for (int i = 0; i < outOfCarSpawns.Length; i++)
        {
            if (outOfCarSpawns[i].GetIsAble())
            {
                spawnPosition = transforms[i].position;
                return;
            }
            else
                spawnPosition = transforms[2].position;
        }
    }
}
