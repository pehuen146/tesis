﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelMovement : MonoBehaviour {

    [SerializeField] [Range(-1, 1)] int direction;

    CarController cc;

    private void Start()
    {
        cc = GetComponentInParent<CarController>();
    }

    private void FixedUpdate()
    {
        if (cc)
            transform.Rotate(cc.GetVelocity() * direction, 0, 0);
    }

}
