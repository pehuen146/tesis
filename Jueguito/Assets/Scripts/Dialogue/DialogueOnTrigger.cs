﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueOnTrigger : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GetComponent<DialogueTrigger>().TriggerDialogue();
            gameObject.SetActive(false);
        }
    }
}
