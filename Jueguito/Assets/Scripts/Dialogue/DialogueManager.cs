﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

    [SerializeField] Text nameTxt;
    [SerializeField] Text dialogueTxt;
    [SerializeField] GameObject dialogueBox;
    [SerializeField] GameObject playerHUD;


    Queue<string> sentences;
    Dialogue dialogue;
    Coroutine characters;

    DialogueTrigger trigger = null;

    bool cr_Running = false;
    string sentence;

    void Awake ()
    {
        sentences = new Queue<string>();
	}

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
            DisplayNextSentence();
    }

    public void StartDialogue(Dialogue _dialogue, DialogueTrigger _trigger)
    {
        dialogueBox.SetActive(true);
        HUDSetActive(false);
        dialogue = _dialogue;
        nameTxt.text = dialogue.name;

        Time.timeScale = 0;

        //Deactivating enemyAI
        EnemyIA[] enemyIAs = FindObjectsOfType<EnemyIA>();

        foreach (var item in enemyIAs)
            item.enabled = false;

        PlayerSingleton.singleton.OnDialog = true;

        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
            sentences.Enqueue(sentence);

        DisplayNextSentence();

        trigger = _trigger;
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0 && !cr_Running)
        {
            EndDialogue();
            return;
        }

        if ( cr_Running)
        {
            StopCoroutine();

            dialogueTxt.text = sentence;
        }
        else
        {
            StopCoroutine();

            dialogueTxt.text = "";

            sentence = sentences.Dequeue();
            characters = StartCoroutine(waitCharacter(sentence));
        }
    }

    IEnumerator waitCharacter(string sentence)
    {
        cr_Running = true;

        dialogueTxt.text = "";

        for (int i = 0; i < sentence.Length; i++)
        {
            dialogueTxt.text += sentence[i];
            yield return new WaitForSecondsRealtime(0.05f);
        }

        cr_Running = false;
    }

    public void EndDialogue()
    {
        if (dialogue.dialogueTrigger)
            dialogue.dialogueTrigger.TriggerDialogue();
        else
        {
            dialogueBox.SetActive(false);
            Time.timeScale = 1;

            //Activating enemyAI
            EnemyIA[] enemyIAs = FindObjectsOfType<EnemyIA>();

            foreach (var item in enemyIAs)
                item.enabled = true;

            PlayerSingleton.singleton.OnDialog = false;

            TriggerEvent triggerEvent = FindObjectOfType<TriggerEvent>();

            if (triggerEvent)
                triggerEvent.Trigger();

            HUDSetActive(true);

            trigger.EndDialogue();
        }
    }

    void StopCoroutine()
    {
        if (characters != null)
        {
            StopCoroutine(characters);
            cr_Running = false;
        }
    }

    public void HUDSetActive(bool setActive)
    {
        playerHUD.SetActive(setActive);
    }
}
