﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogues : MonoBehaviour
{
    DialogueTrigger[] dialogues;

    private void Awake()
    {
        dialogues = GetComponentsInChildren<DialogueTrigger>();
    }

    public DialogueTrigger[] GetDialogues()
    {
        return dialogues;
    }

    public DialogueTrigger GetDialogue(string nameOfDialogue)
    {
        foreach (DialogueTrigger dialogue in dialogues)
        {
            if (dialogue.name == nameOfDialogue)
                return dialogue;
        }

        return null;
    }

}
