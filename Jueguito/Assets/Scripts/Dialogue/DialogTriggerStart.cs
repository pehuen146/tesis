﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTriggerStart : MonoBehaviour {

    DialogueTrigger dialogueTrigger;

    private void Awake()
    {
        dialogueTrigger = GetComponent<DialogueTrigger>();
    }

    private void Start()
    {
        dialogueTrigger.TriggerDialogue();
    }

}
