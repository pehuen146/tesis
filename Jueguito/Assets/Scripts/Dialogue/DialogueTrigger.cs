﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DialogueTrigger : MonoBehaviour {

    [SerializeField] Dialogue dialogue;
    [SerializeField] UnityEvent triggerEvent;
    [SerializeField] UnityEvent triggerEnd;

    public void TriggerDialogue()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue, this);

        triggerEvent.Invoke();
    }

    public void EndDialogue()
    {
        triggerEnd.Invoke();
    }

}
