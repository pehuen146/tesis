﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDT : MonoBehaviour {

    [SerializeField] EnemyData data;

    public string GetName()
    {
        return data.enemyName;
    }

    public float GetDamage()
    {
        return data.damage;
    }

    public float GetSpeed()
    {
        return data.speed;
    }

    public float GetHealth()
    {
        return data.health;
    }

    public float GetVision()
    {
        return data.EnemyVision;
    }

    public float GetFOV()
    {
        return data.fOV;
    }

    public float GetAttackDistance()
    {
        return data.attackDistance;
    }

    public float GetMinimumDistancePatrolPoint()
    {
        return data.minimumDistancePatrolPoint;
    }

    public float GetWaitTime()
    {
        return data.waitTime;
    }

    public float GetAttackTime()
    {
        return data.attackResetTime;
    }

}
