﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    [SerializeField] Transform characterTransform;
    [SerializeField] Transform cameraTransform;
    [SerializeField] GameObject interactionText;
    [SerializeField] float cameraRotationVelocity;
    [SerializeField] float velocity;
    [SerializeField] float runningVelocity;
    [SerializeField] float rotationVelocity;
    [SerializeField] private LayerMask layers;
    [SerializeField] float gravity;
    [SerializeField] float maxResistance;
    [SerializeField] float fatigue;
    [SerializeField] float recovery;
    [SerializeField] ResistanceBarScript resistanceBar;
    [SerializeField] float additionalSpeedPerLevel;
    [SerializeField] int additionalResistancePerLevel;


    CharacterController controller;
    float movementVelocity;
    float horizontal;
    float vertical;
    float verticalAnimation;
    float horizontalAnimation;
    Animator animator;
    LevelManager levelManager;
    RaycastHit hit;
    float resistance;
    float currentMaxResistance;
    Vector3 forwardMovement;
    Vector3 rightMovement;
    bool isFatigue = false;
    float fatiguePrctg;
    bool isRunning = false;
    Color resistanceBarColor;
    ExperienceScript experienceScript;
    Shot shotScript;
    Melee_Script melee;

    Vector3 move;
    Vector3 moveInput;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        levelManager = GetComponent<LevelManager>();
        experienceScript = GetComponent<ExperienceScript>();
        shotScript = GetComponent<Shot>();
        melee = GetComponent<Melee_Script>();
        resistance = maxResistance;
        resistanceBar.UpdateResistanceBar(resistance, maxResistance);
        isFatigue = false;
        fatiguePrctg = maxResistance * 0.3f;        //Calculo el 30 porciento de la barra de vida
        resistanceBarColor = resistanceBar.GetResistanceBar().color;
    }

    void Update()
    {
        if (Time.timeScale == 0 || PlayerSingleton.singleton.OnDialog)
        {
            verticalAnimation = 0;
            horizontalAnimation = 0;
            animator.SetBool("Running", false);

            UpdateAnimator();
            return;
        }

        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        currentMaxResistance = maxResistance + additionalResistancePerLevel * experienceScript.GetCurrentLevel(SkillType.Resistance);

        movementVelocity = velocity;

        if (resistance > 0 && !isFatigue)
        {
            if (Input.GetKey(KeyCode.LeftShift) && (!shotScript.IsReloading() && !melee.IsPunching()) && (horizontal != 0 || vertical != 0))
            {
                float additionalSpeed = additionalSpeedPerLevel * experienceScript.GetCurrentLevel(SkillType.Agility);

                movementVelocity = runningVelocity + additionalSpeed;
                isRunning = true;
                resistance -= fatigue * Time.deltaTime;
                resistanceBar.UpdateResistanceBar(resistance, currentMaxResistance);
            }
            else if (resistance < currentMaxResistance)
            {
                isRunning = false;
                resistance += recovery * Time.deltaTime;
                resistanceBar.UpdateResistanceBar(resistance, currentMaxResistance);
            }
        }
        else
        {
            isFatigue = true;
        }

        if (isRunning)
        {
            Vector3 direction = ((vertical * cameraTransform.forward) + (horizontal * cameraTransform.right)).normalized;
            Quaternion diff = Quaternion.Slerp(characterTransform.rotation, Quaternion.LookRotation(direction), rotationVelocity);

            if (characterTransform.rotation != diff)
            {
                //float yDiff = characterTransform.rotation.eulerAngles.y - diff.eulerAngles.y;

                characterTransform.rotation = diff;
            }
        }
        else if (GetCursorRayHit(out hit, layers))
        {
            Vector3 direction = hit.point - characterTransform.position;
            direction.y = 0;
                
            Quaternion diff = Quaternion.Slerp(characterTransform.rotation, Quaternion.LookRotation(direction), rotationVelocity);

            if (characterTransform.rotation != diff)
            {
                float yDiff = characterTransform.rotation.eulerAngles.y - diff.eulerAngles.y;

                characterTransform.rotation = diff;
                animator.SetFloat("Turn", yDiff);
            }

        }
        
        move = (vertical * cameraTransform.forward) + (horizontal * cameraTransform.right);

        WalkingMove(move);


        if (isFatigue)
        {
            isRunning = false;

            resistanceBar.GetResistanceBar().color = Color.red;
            if (resistance < currentMaxResistance)
            {
                resistance += recovery * Time.deltaTime;
                resistanceBar.UpdateResistanceBar(resistance, currentMaxResistance);
            }
            if (resistance > fatiguePrctg)
            {
                isFatigue = false;
                resistanceBar.GetResistanceBar().color = resistanceBarColor;
            }
        }

        animator.SetBool("Running", isRunning);

        forwardMovement = cameraTransform.forward * vertical * movementVelocity * Time.deltaTime;
        rightMovement = cameraTransform.right * horizontal * movementVelocity * Time.deltaTime;

        controller.Move(forwardMovement);
        controller.Move(rightMovement);

        if (!controller.isGrounded)
            controller.Move(Vector3.down * gravity * Time.deltaTime);
    }

    public bool IsRunning()
    {
        return isRunning;
    }

    public void ResistanceRefill()
    {
        resistance = maxResistance;
        resistanceBar.UpdateResistanceBar(resistance, currentMaxResistance);
    }

    void WalkingMove(Vector3 _move)
    {
        if (_move.magnitude > 1)
            _move.Normalize();

        this.moveInput = _move;

        ConvertMoveInput();
        UpdateAnimator();

    }

    void ConvertMoveInput()
    {
        Vector3 localMove = characterTransform.InverseTransformDirection(moveInput);

        verticalAnimation = localMove.z;
        horizontalAnimation = localMove.x;
    }

    void UpdateAnimator()
    {
        animator.SetFloat("Speed", verticalAnimation * 4, 0f, Time.deltaTime);
        animator.SetFloat("Strafe", horizontalAnimation * 4, 0f, Time.deltaTime);
    }

    public bool GetCursorRayHit(out RaycastHit hit, LayerMask _layers)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        return Physics.Raycast(ray, out hit, Mathf.Infinity, _layers);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Respawn")
         || other.CompareTag("ResourceBox")
         || other.CompareTag("Sign")
         || other.CompareTag("EnterCar")
         || other.CompareTag("Canon"))
        {
            interactionText.SetActive(false);
        }
    }

    private void OnDisable()
    {
        interactionText.SetActive(false);
    }

    void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Respawn")
         || other.CompareTag("ResourceBox")
         || other.CompareTag("Sign")
         || other.CompareTag("EnterCar")
         || other.CompareTag("Canon"))
        {
            if (PlayerSingleton.singleton.OnDialog)
                interactionText.SetActive(false);
            else
                interactionText.SetActive(true);
        }


        if (Input.GetKeyDown(KeyCode.E))
        {
            if (other.CompareTag("Respawn"))
            {
                DoorScript door = other.GetComponent<DoorScript>();

                SaveManager.singleton.Save();

                if (levelManager.GetCurrentLevel() >= door.GetLevel())
                {
                    SceneManagement.singleton.LoadScene(door.GetSceneLinkName(), door.GetDoorIndex());
                    PlayerSingleton.singleton.TogglePlayer(false);
                }
                else if (!PlayerSingleton.singleton.OnDialog)
                    other.GetComponent<DialogueTrigger>().TriggerDialogue();
            }
            else if(other.CompareTag("ResourceBox"))
            {
                other.GetComponent<ResourceBoxScript>().OpenClose();
            }
            else if (other.CompareTag("Sign"))
            {
                other.GetComponent<StreetSign>().ShowHideSign();
            }
        }
    }

    public Transform GetCameraTransform()
    {
        return cameraTransform;
    }
}