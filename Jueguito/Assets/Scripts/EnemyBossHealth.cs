﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBossHealth : MonoBehaviour
{
    [SerializeField] EnemyDT dt;
    [SerializeField] GameObject[] bloodParticles;
    [SerializeField] GameObject audioGO;

    EnemySoundManager soundManager;
    BossAI bIA;
    Animator animator;
    HealthBarScript healthBar;

    float health;

    void Bleed()
    {
        bloodParticles[Random.Range(0, bloodParticles.Length)].SetActive(true);
    }

    void Start()
    {
        health = dt.GetHealth();
        soundManager = GetComponent<EnemySoundManager>();
        bIA = GetComponent<BossAI>();
        animator = GetComponentInChildren<Animator>();
        healthBar = GetComponent<HealthBarScript>();

        UpdateHealthBar();
    }

    public float GetHealth()
    {
        return health;
    }

    public void TakeDamage(float damage)
    {
        //PlayerSingleton.singleton.KillsEnemy();

        health -= damage;

        UpdateHealthBar();

        if (IsAlive())
        {
            bIA.Stun();
            Bleed();
        }
        else
        {
            health = 0;

            if (audioGO)
                audioGO.SetActive(false);
            Invoke("Death", 3);
            soundManager.PlayDeathSound();
            animator.applyRootMotion = true;
            animator.SetTrigger("Death");
            //animator.SetBool("Dead", true);
        }
    }

    public bool IsAlive()
    {
        return health > 0;
    }

    void Death()
    {
        gameObject.SetActive(false);
    }

    void UpdateHealthBar()
    {
        if (healthBar != null)
            healthBar.UpdateHealthBar(health, dt.GetHealth());
    }
}
