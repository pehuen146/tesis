using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

    [SerializeField] int victoryLevel = 4;
    [SerializeField] string[] levelObjectives;
    [SerializeField] Text objectiveText;

    int currentLevel = 0;

    public int GetCurrentLevel()
    {
        return currentLevel;
    }

    public void SetLevel(int level)
    {
        if (level > currentLevel)
        {
            currentLevel = level;

            if (currentLevel <= levelObjectives.Length)
                objectiveText.text = levelObjectives[currentLevel];

            if (level >= victoryLevel)
            {
                Invoke("LoadVictory", 5);
            }
        }
    }

    void LoadVictory()
    {
        SceneManagement.singleton.LoadScene("Ganado");
    }
}
