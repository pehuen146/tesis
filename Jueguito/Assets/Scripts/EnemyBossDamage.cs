﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBossDamage : MonoBehaviour
{
    [SerializeField] EnemyDT dt;
    [SerializeField] string playerTag;
    [SerializeField] Animator animator;
    [SerializeField] bool isBoss;

    const float collisionTime = .5f;

    Timer attackTimer;
    EnemyBossHealth health;
    EnemySoundManager soundManager;

    private void OnEnable()
    {
        attackTimer.Reset();
    }

    void Awake()
    {
        attackTimer = new Timer();
        attackTimer.SetTime(collisionTime, true);
        soundManager = GetComponentInParent<EnemySoundManager>();
        health = GetComponentInParent<EnemyBossHealth>();
    }

    void Update()
    {
        attackTimer.Update();

        if (attackTimer.TimeUp())
            gameObject.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (health.IsAlive())
        {
            if (other.CompareTag(playerTag))
            {
                other.GetComponent<Health>().TakeDamage(dt.GetDamage());
                gameObject.SetActive(false);
            }

            if (other.CompareTag("Canon"))
            {
                if (isBoss)
                    other.GetComponentInParent<Cannon_Script>().CannonDestroy();
                else
                    other.GetComponentInParent<Cannon_Script>().TakeDamage(dt.GetDamage());

                gameObject.SetActive(false);
            }
        }

    }
}
