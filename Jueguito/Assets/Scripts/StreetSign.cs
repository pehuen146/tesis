﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StreetSign : MonoBehaviour
{
    [SerializeField] GameObject camera;
    [SerializeField] float signTime;

    bool collidesWithPlayer = false;

    public void ShowHideSign()
    {
        bool active = !camera.activeInHierarchy;

        camera.SetActive(active);

        if (active)
            Invoke("HideSign", signTime);
    }

    void HideSign()
    {
        camera.SetActive(false);
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            collidesWithPlayer = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            collidesWithPlayer = false;
            camera.SetActive(false);
        }
    }

}
