﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Map : MonoBehaviour {

    [SerializeField] GameObject map;
    [SerializeField] Image mapImage;

    public void ShowMap(Sprite mapTexture)
    {
        map.SetActive(true);
        mapImage.sprite = mapTexture;
    }
}
