﻿using UnityEngine;
using UnityEngine.Events;

public class TriggerEvent : MonoBehaviour {

	[SerializeField] UnityEvent eventToTrigger;

    public void Trigger()
    {
        eventToTrigger.Invoke();
    }
}
