﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class EnemyIA : MonoBehaviour
{
    [SerializeField] Transform sight;
    [SerializeField] EnemyDT dt;
    [SerializeField] LayerMask layers;
    [SerializeField] UnityEvent Death;
    [SerializeField] float stunnedTime;

    NavMeshAgent agent;
    FSM fsm;
    PatrolPoint patrolPoint; // Current patrol point
    Timer waitTimer;
    Timer attackTimer;
    Animator animator;
    EnemyHealth health;
    EnemySoundManager soundManager;
    Transform playerTransform;
    EnemyDamage enemyDamage;
    bool isChase = false;

    private Vector3 previousPosition;
    public float currentSpeed;

    Vector3 lastSeen; // Last time the enemy saw the player
    Vector3 diff;

    void Awake()
    {
        waitTimer = new Timer();
        attackTimer = new Timer();

        attackTimer.SetTime(dt.GetAttackTime(), false);
        waitTimer.SetTime(dt.GetWaitTime(), true);

        agent = GetComponent<NavMeshAgent>();
        health = GetComponent<EnemyHealth>();
        enemyDamage = GetComponentInChildren<EnemyDamage>();
    }


    void Start()
    {
        playerTransform = PlayerSingleton.singleton.GetPlayer().transform;

        waitTimer.SetActive(false);
        attackTimer.SetActive(true);

        if (PatrolManager.singleton)
            patrolPoint = PatrolManager.singleton.GetAndClosePoint();

        animator = GetComponentInChildren<Animator>();
        soundManager = GetComponent<EnemySoundManager>();

        fsm = new FSM(7, 3);

        fsm.SetRelation(State.Idle, 0, State.Chase);
        fsm.SetRelation(State.Chase, 0, State.Attack);
        fsm.SetRelation(State.Chase, 1, State.NextPatrolPoint);
        fsm.SetRelation(State.Stunned, 0, State.Idle);
        fsm.SetRelation(State.NextPatrolPoint, 0, State.Chase);
        fsm.SetRelation(State.NextPatrolPoint, 1, State.InPatrolPoint);
        fsm.SetRelation(State.InPatrolPoint, 0, State.Chase);
        fsm.SetRelation(State.InPatrolPoint, 1, State.NextPatrolPoint);
        fsm.SetRelation(State.InPatrolPoint, 2, State.Idle);
        fsm.SetRelation(State.Attack, 0, State.Idle);
        fsm.SetRelation(State.Attack, 1, State.Chase);
        fsm.SetRelation(State.HeardSomething, 0, State.Idle);

        fsm.SetState(State.NextPatrolPoint);

        agent.speed = dt.GetSpeed();
    }

    void Update()
    {
        if (Time.timeScale <= 0)
            return;

        UpdateMovementAnimation();

        waitTimer.Update();
        attackTimer.Update();

        if (health.IsAlive())
        {
            switch (fsm.GetState())
            {
                case State.Idle:
                    Idle();
                    break;

                case State.Chase:
                    Chase();
                    break;

                case State.InPatrolPoint:
                    InPatrolPoint();
                    break;

                case State.NextPatrolPoint:
                    NextPatrolPoint();
                    break;

                case State.Attack:
                    Attack();
                    break;

                case State.HeardSomething:
                    HeardSomething();
                    break;

                case State.Stunned:
                    Stunned();
                    break;
            }
        }
        else
        {
            IsStopped(true);
            if (isChase)
            {
                isChase = false;
                PlayerSingleton.singleton.StopChaseMusic();
            }
            Death.Invoke();
        }
    }

    void StunnedToIdle()
    {
        fsm.SetState(State.Idle);
    }

    public void Stun()
    {
        fsm.SetState(State.Stunned);

        Invoke("StunnedToIdle", stunnedTime);

        animator.SetTrigger("Hit");
    }

    bool CanSeePlayer()
    {
        if (playerTransform)
        {
            diff = playerTransform.position - sight.position;
            Vector3 dir = diff.normalized;
            float angle = Vector3.Angle(dir, this.transform.forward);
            RaycastHit hit = new RaycastHit();

            if (Physics.Raycast(sight.position, dir, out hit, dt.GetVision(), layers))
            {
                Debug.DrawRay(sight.position, dir * (sight.position - hit.point).magnitude, Color.red, .01f);


                //Debug.DrawRay(sight.position, playerTransform.position - sight.position, Color.red, .5f);

                if (hit.collider.CompareTag("Player") && (diff.magnitude < 10 || angle < dt.GetFOV()))
                    return true;
            }
        }
        return false;
    }

    void IsStopped(bool value)
    {
        agent.isStopped = value;
    }

    public void HearSound(Vector3 soundPosition)
    {
        lastSeen = soundPosition;
        fsm.SetState(State.HeardSomething);
        agent.SetDestination(soundPosition);
    }

    void Attack()
    {
        if (!health.IsAlive() || !playerTransform)
            return;
        
        if (CanSeePlayer())
        {
            agent.SetDestination(playerTransform.position);
            diff = playerTransform.position - transform.position;

            if (diff.magnitude <= dt.GetAttackDistance())
            {
                if (attackTimer.TimeUp())
                {
                    //enemyDamage.gameObject.SetActive(true);
                    animator.SetTrigger("Attack");
                    soundManager.PlayAttackSound();
                    attackTimer.Reset();
                }
            }
            else
                fsm.SetEvent(1);
        }
        else
            fsm.SetEvent(0);
    }

    void Idle()
    {
        IsStopped(true);

        if (CanSeePlayer())
        {
            soundManager.PlayCanSeePlayerSound();
            fsm.SetEvent(0);
        }
    }

    void Chase()
    {
        IsStopped(false);

        if (!isChase)
        {
            PlayerSingleton.singleton.PlayChaseMusic();
            isChase = true;
        }

        if (CanSeePlayer())
        {
            lastSeen = playerTransform.position;
            diff = playerTransform.position - transform.position;

            agent.SetDestination(playerTransform.position);
            if (diff.magnitude < dt.GetAttackDistance())
                fsm.SetEvent(0);
        }
        else if (Vector3.Magnitude(transform.position - lastSeen) < dt.GetAttackDistance())
        {
            IsStopped(true);
            waitTimer.SetActive(true);
            if (waitTimer.TimeUp())
            {
                waitTimer.Reset();
                waitTimer.SetActive(false);
                isChase = false;
                PlayerSingleton.singleton.StopChaseMusic();
                fsm.SetEvent(1);
            }
        }
    }

    void InPatrolPoint()
    {
        if (CanSeePlayer())
        {
            soundManager.PlayCanSeePlayerSound();
            fsm.SetEvent(0);
            waitTimer.Reset();
            waitTimer.SetActive(false);
        }
        else
        {
            IsStopped(true);

            waitTimer.SetActive(true);

            if (!patrolPoint)
                fsm.SetEvent(2);
            else if (waitTimer.TimeUp())
            {
                PatrolManager.singleton.OpenPoint(patrolPoint);
                patrolPoint = PatrolManager.singleton.GetAndClosePoint();
                waitTimer.Reset();
                waitTimer.SetActive(false);
                fsm.SetEvent(1);
            }
        }
    }

    void NextPatrolPoint()
    {
        IsStopped(false);

        if (CanSeePlayer())
        {
            soundManager.PlayCanSeePlayerSound();
            fsm.SetEvent(0);
        }
        else if (!patrolPoint || Vector3.Magnitude(transform.position - patrolPoint.GetPosition()) < dt.GetMinimumDistancePatrolPoint())
        {
            fsm.SetEvent(1);
        }
        else
            agent.SetDestination(patrolPoint.transform.position);
    }

    void HeardSomething()
    {
        if (CanSeePlayer())
            fsm.SetEvent(0);

        IsStopped(false);
        agent.SetDestination(lastSeen);
    }

    void Stunned()
    {
        IsStopped(true);
    }

    void UpdateMovementAnimation()
    {
        float previousSpeed = currentSpeed;

        Vector3 curMove = transform.position - previousPosition;
        currentSpeed = curMove.magnitude / Time.deltaTime;
        previousPosition = transform.position;

        currentSpeed = Mathf.Lerp(previousSpeed, currentSpeed, 0.1f);

        animator.SetFloat("Speed", currentSpeed);
    }
}

public enum State { Idle, Chase, InPatrolPoint, NextPatrolPoint, Attack, HeardSomething, Stunned };