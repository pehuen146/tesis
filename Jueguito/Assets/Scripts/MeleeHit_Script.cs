﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeHit_Script : MonoBehaviour
{
    [SerializeField] Melee_Script meleeScript;
    [SerializeField] string enemyTag;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(enemyTag))
        {
            EnemyHealth health = other.GetComponent<EnemyHealth>();
            if (health)
                health.TakeDamage(meleeScript.GetMeleeDamage());
        }
    }

}
