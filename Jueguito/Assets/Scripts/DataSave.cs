﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataSave
{
    public SerializableVector3 playerPosition;
    public SerializableQuaternion playerRotation;
    public int playerLevel;
    public int equipedWeaponIndex;
    public int bulletsEquiped;
    public int experiencePoints;
    public int[] skills;
    public float experience;
    public float playerHealth;
    public string sceneName;
    public List<SceneDataSave> scenes = new List<SceneDataSave>();
    public List<ItemDataSave> inventoryItems = new List<ItemDataSave>();
    public bool flaslightActivated = false;
}

[Serializable]
public class SceneDataSave
{
    public string sceneName;
    public List<BoxDataSave> boxes = new List<BoxDataSave>();
    public List<SerializableTransfom> saveTransforms = new List<SerializableTransfom>();
}

[Serializable]
public class BoxDataSave
{
    public List<ItemDataSave> items=new List<ItemDataSave>();
}

[Serializable]
public class ItemDataSave
{
    public int index;
    public int stash;

    public ItemDataSave(int _index, int _stash)
    {
        index = _index;
        stash = _stash;
    }
}

public class ItemSaved
{
    public ItemsData itemData;
    public int stash;
}

