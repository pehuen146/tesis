﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKHandling : MonoBehaviour {
    
    [SerializeField] float leftHandWeight = 1;
    [SerializeField] Transform leftHandTarget;
    [SerializeField] float rightHandWeight = 1;
    [SerializeField] Transform rightHandTarget;
    [SerializeField] Transform weapon;
    [SerializeField] Vector3 lookPos;

    Animator anim;
    
    void Start ()
    {
        anim = GetComponent<Animator>();
    }

    private void OnAnimatorIK(int layerIndex)
    {
        anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, leftHandWeight);
        anim.SetIKPosition(AvatarIKGoal.LeftHand, leftHandTarget.position);
        anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, leftHandWeight);
        anim.SetIKRotation(AvatarIKGoal.LeftHand, leftHandTarget.rotation);


        anim.SetIKPositionWeight(AvatarIKGoal.RightHand, rightHandWeight);
        anim.SetIKPosition(AvatarIKGoal.RightHand, rightHandTarget.position);
        anim.SetIKRotationWeight(AvatarIKGoal.RightHand, rightHandWeight);
        anim.SetIKRotation(AvatarIKGoal.RightHand, rightHandTarget.rotation);
    }
}
