﻿using UnityEngine;

public class BulletScript : MonoBehaviour
{
    [SerializeField] string enemyTag;
    [SerializeField] string environmentTag;
    [SerializeField] GameObject bulletHolePrefab;
    [SerializeField] LayerMask layers;

    BulletsData bulletsData;

    float timer;

    void Update()
    {
        Vector3 origin = transform.position;

        transform.Translate(0, 0, bulletsData.GetSpeed() * Time.deltaTime);

        Vector3 direction = transform.position - origin;

        Ray ray = new Ray(origin, direction);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, direction.magnitude, layers))
            OnHit(hit);

        if (timer > 0)
            timer -= Time.deltaTime;

        if (timer <= 0)
            GetComponent<PoolObject>().Recycle();
    }

    void OnHit(RaycastHit hit)
    {
        if (hit.transform.CompareTag(enemyTag))
        {
            GetComponent<PoolObject>().Recycle();
            EnemyHealth health = hit.transform.GetComponent<EnemyHealth>();
            if (health)
                health.TakeDamage(bulletsData.GetDamage());
            else
                hit.transform.GetComponent<EnemyBossHealth>().TakeDamage(bulletsData.GetDamage());
        }
        else if (hit.transform.CompareTag(environmentTag))
        {
            GetComponent<PoolObject>().Recycle();

            Instantiate<GameObject>(bulletHolePrefab, hit.point, Quaternion.LookRotation(hit.normal));
        }
    }

    public void SetData(BulletsData _bulletsData)
    {
        bulletsData = _bulletsData;

        if (bulletsData)
            timer = bulletsData.GetMaxTime();
    }

}
