﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallTriggerTutorial : MonoBehaviour
{
    [SerializeField] GameObject enemy;
    [SerializeField] Animator fenceAnim;

    private void Update()
    {
        if (!enemy.activeInHierarchy)
            fenceAnim.SetTrigger("Open");
    }
}
