﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelPlayerSetter : MonoBehaviour {

    [SerializeField] int level;
    
    public void SetLevel()
    {
        PlayerSingleton.singleton.GetComponent<LevelManager>().SetLevel(level);
    }
}
