﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;

public class TimelineTrigger : MonoBehaviour {

    [SerializeField] bool playOnStart;
    [SerializeField] bool playOnTrigger;
    [SerializeField] bool ActivateDeactivatePlayer;
    [SerializeField] UnityEvent timelineEnd;
    [SerializeField] int ActivationLevel;

    PlayableDirector timeline;

    private void Awake()
    {
        timeline = GetComponent<PlayableDirector>();

        //Setting timeline end trigger
        timeline.stopped += Timeline_stopped;
        timeline.played += Timeline_played;
    }

    private void Start()
    {

        if (playOnStart && PlayerSingleton.singleton != null)
        {
            LevelManager levelManager = PlayerSingleton.singleton.GetComponent<LevelManager>();

            if (levelManager.GetCurrentLevel() <= ActivationLevel)
                timeline.Play();
        }
    }

    private void Timeline_played(PlayableDirector obj)
    {
        if (ActivateDeactivatePlayer && PlayerSingleton.singleton!= null)
            PlayerSingleton.singleton.gameObject.SetActive(false);
        PlayerSingleton.singleton.GetComponentInChildren<DialogueManager>().HUDSetActive(false);
    }

    private void Timeline_stopped(PlayableDirector obj)
    {
        if (ActivateDeactivatePlayer && PlayerSingleton.singleton != null)
            PlayerSingleton.singleton.gameObject.SetActive(true);

        PlayerSingleton.singleton.GetComponentInChildren<DialogueManager>().HUDSetActive(true);

        timelineEnd.Invoke();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (playOnTrigger && other.CompareTag("Player"))
            timeline.Play();
    }
}
