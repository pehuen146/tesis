﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour {

    Transform cameraTransform;

    void Start()
    {
        cameraTransform = Camera.main.transform;
    }

	void Update ()
    {
        if (cameraTransform)
            transform.rotation = Quaternion.LookRotation(transform.position - cameraTransform.position);
	}
}
