﻿using UnityEngine.UI;
using UnityEngine;

public class NoteScript : MonoBehaviour {

    [SerializeField] GameObject note;
    [SerializeField] Text noteText;
    [SerializeField] Text keyAddress;

    public void OnClickButtonClose()
    {
        note.SetActive(false);
    }

    public void SetKeyAddress(string _keyAddress)
    {
        _keyAddress = _keyAddress.Replace("@", System.Environment.NewLine);

        keyAddress.text = _keyAddress;
    }

    public void SetText(string _noteText)
    {
        _noteText = _noteText.Replace("@", System.Environment.NewLine);

        noteText.text = _noteText;
    }

    public void OpenNote()
    {
        note.SetActive(true);
    }
}
