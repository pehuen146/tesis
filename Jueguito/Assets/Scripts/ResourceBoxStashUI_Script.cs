﻿using UnityEngine;
using UnityEngine.UI;

public class ResourceBoxStashUI_Script : MonoBehaviour {

    [SerializeField] Text stashUI_txt;
    [SerializeField] ItemScript itm;

    private void Update()
    {
        if (itm.GetData())
            stashUI_txt.text = "x" + itm.GetStash();
        else
            stashUI_txt.text = "x0";
    }

}
