﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolManager : MonoBehaviour {

    public static PatrolManager singleton;
    
    [SerializeField] PatrolPoint[] patrolPoints;

    List<PatrolPoint> openPoints = new List<PatrolPoint>();
    
    void Awake()
    {
        //--------------------------------------------------------
        if (singleton != null)
        {
            Debug.LogError("Patrol manager duplicado", gameObject);
            Destroy(gameObject);
        }
        else
            singleton = this;
        //--------------------------------------------------------

        for (int i = 0; i < patrolPoints.Length; i++)
        {
            openPoints.Add(patrolPoints[i]);
        }
    }

    public PatrolPoint GetAndClosePoint()
    {
        if (openPoints.Count > 0)
        {
            int auxIndex = Random.Range(0, openPoints.Count - 1);
            PatrolPoint auxPoint = openPoints[auxIndex];
            openPoints.Remove(auxPoint);
            return auxPoint;
        }
        return null;
    }

    public void OpenPoint(PatrolPoint point)
    {
        openPoints.Add(point);
    }
}
