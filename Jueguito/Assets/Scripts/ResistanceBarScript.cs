﻿using UnityEngine.UI;
using UnityEngine;

public class ResistanceBarScript : MonoBehaviour {

    [SerializeField] Image resistanceBar;
    [SerializeField] float defaultSizeDeltaX;

    public void UpdateResistanceBar(float resistance, float maxResistance)
    {
        Vector2 barSize = new Vector2((defaultSizeDeltaX * resistance) / maxResistance, resistanceBar.rectTransform.sizeDelta.y);

        resistanceBar.rectTransform.sizeDelta = barSize;
    }

    public Image GetResistanceBar()
    {
        return resistanceBar;
    }

}
