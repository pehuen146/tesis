using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSwitch : MonoBehaviour {
    
    [SerializeField] GameObject pistol;
    [SerializeField] GameObject pistolShotParticles;
    [SerializeField] Transform pistolBulletSpawn;
    [SerializeField] GameObject ak_47;
    [SerializeField] GameObject ak_47ShotParticles;
    [SerializeField] Transform ak_47BulletSpawn;
    [SerializeField] GameObject shotgun;
    [SerializeField] GameObject shotgunShotParticles;
    [SerializeField] Transform shotgunBulletSpawn;
    [SerializeField] GameObject knife;

    public void ChangeWeapon(WeaponsData.WeaponType weaponType, Animator animator, Shot shotScript)
    {
        switch (weaponType)
        {
		 case WeaponsData.WeaponType.Knife:
                pistol.SetActive(false);
                ak_47.SetActive(false);
                shotgun.SetActive(false);
                animator.SetBool("Pistol", false);
                animator.SetBool("Rifle", false);
                shotScript.SetShotParticles(null);
                shotScript.SetBulletSpawn(null);
			knife.SetActive(true);
		 break;
            case WeaponsData.WeaponType.Empty:
                pistol.SetActive(false);
                ak_47.SetActive(false);
                shotgun.SetActive(false);
                animator.SetBool("Pistol", false);
                animator.SetBool("Rifle", false);
                shotScript.SetShotParticles(null);
                shotScript.SetBulletSpawn(null);
                break;
            case WeaponsData.WeaponType.Pistol:
                pistol.SetActive(true);
                ak_47.SetActive(false);
                shotgun.SetActive(false);
                animator.SetBool("Pistol", true);
                animator.SetBool("Rifle", false);
                shotScript.SetShotParticles(pistolShotParticles);
                shotScript.SetBulletSpawn(pistolBulletSpawn);
                break;
            case WeaponsData.WeaponType.Rifle:
                pistol.SetActive(false);
                ak_47.SetActive(true);
                shotgun.SetActive(false);
                animator.SetBool("Pistol", false);
                animator.SetBool("Rifle", true);
                shotScript.SetShotParticles(ak_47ShotParticles);
                shotScript.SetBulletSpawn(ak_47BulletSpawn);
                break;
            case WeaponsData.WeaponType.Shotgun:
                pistol.SetActive(false);
                ak_47.SetActive(false);
                shotgun.SetActive(true);
                animator.SetBool("Pistol", false);
                animator.SetBool("Rifle", true);
                shotScript.SetShotParticles(shotgunShotParticles);
                shotScript.SetBulletSpawn(shotgunBulletSpawn);
                break;
        }
    }
}
