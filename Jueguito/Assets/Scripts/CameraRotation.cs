﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    [SerializeField] float cameraRotationVelocity;

    private void Update()
    {


        transform.rotation = Quaternion.LookRotation(transform.forward, Vector3.up);

        if (Input.GetMouseButton(1) && Time.timeScale != 0)
            transform.Rotate(0, Input.GetAxis("Mouse X") * cameraRotationVelocity, 0);

    }
}
