﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivatePlayer : MonoBehaviour
{
    GameObject playerGO;

    private void Start()
    {
        playerGO = PlayerSingleton.singleton.gameObject;
    }

    public void Deactivate(bool activate)
    {
        playerGO.SetActive(activate);
    }
}
