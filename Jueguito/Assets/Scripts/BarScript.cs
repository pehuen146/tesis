﻿using UnityEngine.UI;
using UnityEngine;

public class BarScript : MonoBehaviour {

    [SerializeField] GameObject barGO;

    Slider slider;

    private void Awake()
    {
        slider = barGO.GetComponent<Slider>();
    }

    public void UpdateBar(float current, float max)
    {
        slider.value = current / max;
    }

    public GameObject GetBarGO()
    {
        return barGO;
    }
}
