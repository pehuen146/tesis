﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;

public class Shot : MonoBehaviour
{
    [SerializeField] ItemScript currentFireWeapon;
    [SerializeField] private LayerMask layers;
    [SerializeField] BulletsUI bulletsUI;
    [SerializeField] InventoryScript inventoryScript;
    [SerializeField] float shotParticlesTime;
    [SerializeField] WeaponSwitch weaponSwitch;
    [SerializeField] Text AmmoStatus;

    Timer timer;
    Timer reloadTimer;
    Timer shotParticleTimer;

    float lessReloadTimePerLevel;

    ExperienceScript experienceScript;
    WeaponsData weaponData;
    BulletsData bulletsData;
    Transform bulletSpawn;
    PlayerSoundManager soundManager;
    PlayerMovement playerMovement;
    GameObject shotParticles;
    Animator animator;
    Pool pool;
    int charger = 0;
    int bulletStash = 0;

    bool reloading = false;

    private void Awake()
    {
        timer = new Timer();
        reloadTimer = new Timer();
        shotParticleTimer = new Timer();
        animator = GetComponent<Animator>();
        weaponSwitch.ChangeWeapon(WeaponsData.WeaponType.Empty, animator, this);
        experienceScript = GetComponent<ExperienceScript>();
    }

    void Start()
    {
        //shotParticles.SetActive(false);
        shotParticleTimer.SetTime(shotParticlesTime, false);
        soundManager = GetComponent<PlayerSoundManager>();
        playerMovement = GetComponent<PlayerMovement>();
    }

    private void OnEnable()
    {
        if (weaponData)
            weaponSwitch.ChangeWeapon(weaponData.weaponType, animator, this);
    }

    void Update()
    {
        timer.Update();
        reloadTimer.Update();
        shotParticleTimer.Update();

        if (weaponData)
        {
            if (!playerMovement.IsRunning() && Input.GetKey(KeyCode.Mouse0) && timer.TimeUp() && charger > 0 && !reloadTimer.GetActive())
            {
                pool = PoolManager.GetInstance().GetPool("BulletPool");

                RaycastHit hit;
                if (!EventSystem.current.IsPointerOverGameObject() && playerMovement.GetCursorRayHit(out hit, layers))
                {
                    PoolObject po = pool.GetPooledObject();
                    po.gameObject.transform.position = bulletSpawn.position;
                    po.gameObject.transform.LookAt(new Vector3(hit.point.x, bulletSpawn.position.y, hit.point.z));
                    po.gameObject.GetComponent<BulletScript>().SetData(bulletsData);    //testear

                    if (weaponData.weaponType == WeaponsData.WeaponType.Shotgun)
                    {
                        PoolObject po2 = pool.GetPooledObject();
                        PoolObject po3 = pool.GetPooledObject();

                        po2.gameObject.transform.position = bulletSpawn.position;
                        po2.gameObject.transform.LookAt(new Vector3(hit.point.x, bulletSpawn.position.y, hit.point.z));
                        po2.transform.Rotate(0, 5, 0);
                        
                        po3.gameObject.transform.position = bulletSpawn.position;
                        po3.gameObject.transform.LookAt(new Vector3(hit.point.x, bulletSpawn.position.y, hit.point.z));
                        po3.transform.Rotate(0, -5, 0);

                        po2.gameObject.GetComponent<BulletScript>().SetData(bulletsData);
                        po3.gameObject.GetComponent<BulletScript>().SetData(bulletsData);
                    }


                    soundManager.PlayGunShotSound(weaponData.GetGunShotSounds());
                    timer.Reset();
                    shotParticleTimer.Reset();
                    charger--;
                    
                    animator.SetTrigger("Recoil");

                    ShowAmmoStatus();
                    bulletsUI.SetCurrentAmmo(charger);
                }
            }

            if (shotParticleTimer.TimeUp())
                shotParticles.SetActive(false);
            else
                shotParticles.SetActive(true);

            if (Input.GetKeyDown(KeyCode.R) && weaponData.chargerCapacity > charger && bulletStash > 0)
            {
                reloadTimer.SetActive(true);

                if (weaponData.weaponType == WeaponsData.WeaponType.Shotgun)
                    animator.SetTrigger("ShotgunReload");
                else
                    animator.SetTrigger("Reload");

                reloading = true;

                soundManager.PlaySound(weaponData.GetReloadSound());

                lessReloadTimePerLevel = weaponData.reloadTime / (experienceScript.GetMaxSkillLevel() + 1);
                reloadTimer.SetTime(weaponData.reloadTime - (lessReloadTimePerLevel * experienceScript.GetCurrentLevel(SkillType.ReloadVelocity)), true);
            }

            if (reloadTimer.TimeUp())
            {

                reloading = false;

                if (weaponData.GetAmmo())
                {
                    bulletsData = (BulletsData)weaponData.GetAmmo().GetData();
                    charger += weaponData.GetAmmo().GetStash();
                    if (charger > weaponData.chargerCapacity)
                    {
                        weaponData.GetAmmo().SetStash(charger - weaponData.chargerCapacity);
                        charger = weaponData.chargerCapacity;
                        bulletStash = weaponData.GetAmmo().GetStash();
                    }
                    else
                    {
                        weaponData.GetAmmo().SetStash(0);
                        weaponData.GetAmmo().SetData(null);
                        weaponData.GetAmmo().gameObject.SetActive(false);
                        weaponData.SetAmmo(null);
                        bulletStash = 0;
                    }
                }

                AmmoStatus.gameObject.SetActive(false);

                bulletsUI.SetCurrentAmmo(charger);
                reloadTimer.Reset();
                reloadTimer.SetActive(false);
            }
            if (weaponData.GetAmmo())
                bulletStash = weaponData.GetAmmo().GetStash();
            bulletsUI.SetMaxAmmo(bulletStash);
        }
    }

    public void ShowAmmoStatus()
    {
        if (charger == 0 && weaponData)
        {
            AmmoStatus.gameObject.SetActive(true);

            if (bulletStash == 0)
                AmmoStatus.text = "Sin munición";
            else
                AmmoStatus.text = "Pulsa R para recargar";
        }
    }

    public void SetData(WeaponsData _weaponData)
    {
        if (currentFireWeapon.GetData())
        {
            if (bulletsData /*&& weaponData.GetAmmo()*/)
                inventoryScript.AddItems(bulletsData, charger);
            charger = 0;
            bulletsUI.SetMaxAmmo(0);
            bulletsUI.SetCurrentAmmo(charger);
            timer.SetActive(false);
            currentFireWeapon.SetData(null);
        }

        weaponData = _weaponData;

        if (weaponData)
        {
            if (weaponData.GetAmmo())
            {
                bulletsData = (BulletsData)weaponData.GetAmmo().GetData();
                charger += weaponData.GetAmmo().GetStash();
                if (charger > weaponData.chargerCapacity)
                {
                    weaponData.GetAmmo().SetStash(charger - weaponData.chargerCapacity);
                    charger = weaponData.chargerCapacity;
                    bulletStash = weaponData.GetAmmo().GetStash();
                }
                else
                {
                    weaponData.GetAmmo().SetStash(0);
                    weaponData.GetAmmo().SetData(null);
                    weaponData.GetAmmo().gameObject.SetActive(false);
                    bulletStash = 0;
                    weaponData.SetAmmo(null);
                }

                bulletsUI.SetMaxAmmo(bulletStash);
                bulletsUI.SetCurrentAmmo(charger);
            }
            timer.SetTime(weaponData.shotCooldown, true);
            timer.SetActive(true);
            reloadTimer.SetTime(weaponData.reloadTime, true);
            reloadTimer.SetActive(false);
            currentFireWeapon.SetData(_weaponData);
            weaponSwitch.ChangeWeapon(weaponData.weaponType, animator, this);
        }
        else
        {
            charger = 0;
            bulletStash = 0;
            bulletsUI.SetMaxAmmo(0);
            bulletsUI.SetCurrentAmmo(charger);
            timer.SetActive(false);
            currentFireWeapon.SetData(null);
            weaponSwitch.ChangeWeapon(WeaponsData.WeaponType.Empty, animator, this);
        }

        AmmoStatus.gameObject.SetActive(false);

        ShowAmmoStatus();
    }

    public BulletsData GetBulletsData()
    {
        return bulletsData;
    }

    public bool IsReloading()
    {
        return reloading;
    }

    public int GetCharger()
    {
        return charger;
    }

    public ItemScript GetCurrentWeapon()
    {
        return currentFireWeapon;
    }

    public WeaponsData GetData()
    {
        return weaponData;
    }

    public void SetShotParticles(GameObject _ShotParticles)
    {
        shotParticles = _ShotParticles;
    }

    public void SetBulletSpawn(Transform _bulletSpawn)
    {
        bulletSpawn = _bulletSpawn;
    }

    public void UpdateBullets()
    {
        if (weaponData)
        {
            List<ItemScript> invItms = inventoryScript.GetInventoryItems();
            for (int i = 0; i < invItms.Capacity; i++)
                if (invItms[i].GetData())
                    if (invItms[i].GetData().index == weaponData.GetBulletIndex())
                    {
                        weaponData.SetAmmo(invItms[i]);
                        bulletStash = weaponData.GetAmmo().GetStash();
                    }
        }

        ShowAmmoStatus();
    }

    public int GetAmmo()
    {
        return charger;
    }

    public void SetAmmo(int ammoAmount)
    {
        charger = ammoAmount;
    }
}
