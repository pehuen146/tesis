﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceBoxScript : MonoBehaviour
{
    [SerializeField] GameObject resourceBoxInventory;
    [SerializeField] List<ItemScript> items;

    InventoryScript inventoryScript;
    List<ItemScript> inventoryItems;
    InventoryUI inventoryUI;
    Shot shotScript;

    private void Awake()
    {
        resourceBoxInventory.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        inventoryScript = other.GetComponent<InventoryScript>();
        if (inventoryScript)
        {
            inventoryItems = inventoryScript.GetInventoryItems();
            inventoryUI = inventoryScript.GetComponentInChildren<InventoryUI>();
        }
        shotScript = other.GetComponent<Shot>();
    }

    public void OpenClose()
    {
        bool boxActive = !resourceBoxInventory.activeSelf;

        resourceBoxInventory.SetActive(boxActive);
        
        inventoryUI.ActivateInventory(boxActive);

        if (resourceBoxInventory.activeInHierarchy)
            {
                for (int i = 0; i < items.Capacity; i++)
                    items[i].SetInventory(inventoryScript);

                for (int i = 0; i < inventoryItems.Capacity; i++)
                    inventoryItems[i].SetResourceBox(this);
            }
            else
                for (int i = 0; i < inventoryItems.Capacity; i++)
                    inventoryItems[i].SetResourceBox(null);
    }

    private void OnTriggerExit(Collider other)
    {
        resourceBoxInventory.SetActive(false);
        inventoryUI.ActivateInventory(false);

        for (int i = 0; i < inventoryItems.Capacity; i++)
            if (inventoryItems[i].GetResourceBox())
                inventoryItems[i].SetResourceBox(null);
    }

    public void AddItems(ItemsData data, ItemScript invtItm)
    {
        for (int i = 0; i < items.Capacity; i++)
        {
            if (!items[i].GetData())
            {
                items[i].SetData(data);
                items[i].gameObject.SetActive(true);
                inventoryScript.WeightSubstraction(data.weight);
                invtItm.StashSubstraction(1);
                items[i].SetStash(1);

                if (invtItm.GetStash() <= 0)
                {
                    if (shotScript.GetCurrentWeapon().GetData())
                    {
                        int weaponSlotIndex = shotScript.GetCurrentWeapon().GetData().index;
                        if (invtItm.GetData().index > 3 && invtItm.GetData().index < 7 && weaponSlotIndex == invtItm.GetData().index)
                        {
                            if (shotScript.GetBulletsData())
                                inventoryScript.AddItems(shotScript.GetBulletsData(), shotScript.GetCharger());
                            shotScript.GetCurrentWeapon().SetData(null);
                            shotScript.SetData(null);
                        }
                    }

                    invtItm.gameObject.SetActive(false);
                    invtItm.SetData(null);
                    invtItm.SetInventory(null);
                }
                return;
            }
            else if (items[i].GetData().index == data.index && data.index != 3)
            {
                inventoryScript.WeightSubstraction(data.weight);
                invtItm.StashSubstraction(1);
                items[i].StashAdd(1);

                if (invtItm.GetStash() <= 0)
                {
                    invtItm.gameObject.SetActive(false);
                    invtItm.SetData(null);
                    invtItm.SetInventory(null);
                }
                return;
            }
        }
    }

    public void AddItems(ItemsData data)
    {
        for (int i = 0; i < items.Capacity; i++)
        {
            if (!items[i].GetData())
            {
                items[i].SetData(data);
                items[i].gameObject.SetActive(true);
                return;
            }
        }
    }

    public void AddItems(List<ItemSaved> itemsSaved)
    {
        EmptyBox();

        for (int i = 0; i < itemsSaved.Count; i++)
        {
            items[i].SetData(itemsSaved[i].itemData);
            items[i].SetStash(itemsSaved[i].stash);
            
            items[i].gameObject.SetActive(true);
        }
    }

    public void EmptyBox()
    {
        for (int i = 0; i < items.Count; i++)
        {
            items[i].SetData(null);
            items[i].gameObject.SetActive(false);
        }
    }

    public List<ItemScript> GetItems()
    {
        return items;
    }

}
