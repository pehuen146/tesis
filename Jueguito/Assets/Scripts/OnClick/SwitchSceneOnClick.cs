﻿using UnityEngine;
using UnityEngine.UI;

public class SwitchSceneOnClick : MonoBehaviour
{
    [SerializeField] string SceneName;

    Button button;

    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(LoadScene);
    }

    void LoadScene()
    {
        SceneManagement.singleton.LoadScene(SceneName);
    }
}
