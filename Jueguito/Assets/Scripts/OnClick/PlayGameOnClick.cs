﻿using UnityEngine;
using UnityEngine.UI;

public class PlayGameOnClick : MonoBehaviour
{

    [SerializeField] bool newGame;
    Button button;

    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(LoadScene);
    }

    void LoadScene()
    {
        if (newGame)
            SaveManager.singleton.DeleteSave();
        else
            SaveManager.singleton.SetGameContinued(true);


        SceneManagement.singleton.LoadGame(newGame);
    }
}
