﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchScene : MonoBehaviour {
    
    public void LoadScene(string sceneName)
    {
        SceneManagement.singleton.LoadScene(sceneName);
    }

    public void NextScene()
    {
        SceneManagement.singleton.NextScene();
    }

    public void LoadMenu()
    {
        SceneManagement.singleton.LoadMenu();
    }

}
