﻿using UnityEngine;
using UnityEngine.UI;

public class AddXPLevelOnClick : MonoBehaviour {

    [SerializeField]
    string skillName;
    [SerializeField]
    SkillType skill;

    ExperienceScript XPScript;
    Button button;
    Text buttonText;

    void Awake()
    {
        button = GetComponent<Button>();
        buttonText = button.GetComponentInChildren<Text>();
        XPScript = GetComponentInParent<ExperienceScript>();
        button.onClick.AddListener(CallExperienceAdd);

        UpdateUILevel();
    }

    private void Start()
    {
        UpdateUILevel();
    }

    void CallExperienceAdd()
    {
        XPScript.UseExperiencePoint(skill);
        UpdateUILevel();
    }

    void UpdateUILevel()
    {
        buttonText.text = skillName + " (" + XPScript.GetCurrentLevel(skill).ToString() + "/"
                                                   + XPScript.GetMaxSkillLevel() + ")";
    }
}
