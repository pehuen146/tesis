﻿using UnityEngine;
using UnityEngine.UI;

public class CloseGameOnClick : MonoBehaviour {

    Button button;

    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(CloseGame);
    }

    public void CloseGame()
    {
        Application.Quit();
    }
}
