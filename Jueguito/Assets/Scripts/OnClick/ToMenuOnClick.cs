using UnityEngine;
using UnityEngine.UI;

public class ToMenuOnClick : MonoBehaviour {
    Button button;

    void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(LoadScene);
    }

    void LoadScene()
    {
        
	   Destroy(PlayerSingleton.singleton.gameObject);
		SceneManagement.singleton.LoadMenu();
    }
}
