﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class ToggleEvent : UnityEvent<bool> { }

public class FlashLight : MonoBehaviour {

    [SerializeField] Light flashLight;
    [SerializeField] Light ambientLight;
    [SerializeField] BarScript barScript;
    [SerializeField] float batteryMaxCapacity = 100;
    [SerializeField] float batterySubstraction = 0.1f;
    [SerializeField] float batteryAddition;
    [SerializeField] AudioClip flashlightClip;
    [SerializeField] float lightIntensity = 5.0f;

    [SerializeField] ToggleEvent switchEvent;

    AudioSource audioSource;
    float batteryCapacity;
    float batteryPrctg;

    private void Start()
    {
        audioSource = GetComponentInParent<AudioSource>();
        batteryCapacity = batteryMaxCapacity;

        SetLightActive(false);

        batteryPrctg = batteryMaxCapacity * 0.1f;
        flashLight.intensity = lightIntensity;
    }

    private void Update()
    {
        if (batteryCapacity < batteryPrctg)
            flashLight.intensity -= 1 * Time.deltaTime;

        if (batteryCapacity > 0)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {                
                Switch();
            }
        }
        else
        {
            SetLightActive(false);
            flashLight.intensity = lightIntensity;
            batteryCapacity = 0;
        }

        if (flashLight.enabled)
        {
            batteryCapacity -= batterySubstraction * Time.deltaTime;
            barScript.UpdateBar(batteryCapacity, batteryMaxCapacity);
        }
        else if (batteryCapacity != batteryMaxCapacity)
            ChargeBattery();
    }

    void Switch()
    {
        bool value = !flashLight.enabled;

        audioSource.PlayOneShot(flashlightClip);
        switchEvent.Invoke(value);

        barScript.GetBarGO().SetActive(value);
    }

    void SetLightActive(bool value)
    {
        switchEvent.Invoke(value);
        barScript.GetBarGO().SetActive(value);
    }

    public void ChargeBattery()
    {
        batteryCapacity += batteryAddition * Time.deltaTime;
        if (batteryCapacity > batteryMaxCapacity)
            batteryCapacity = batteryMaxCapacity;
    }

}
