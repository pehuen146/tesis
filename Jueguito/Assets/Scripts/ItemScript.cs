﻿using UnityEngine;
using UnityEngine.UI;

public class ItemScript : MonoBehaviour
{
    [SerializeField] ItemsData data;
    [SerializeField] GameObject inventoryOptions;

    InventoryUI inventoryUI;
    Image itmImage;
    InventoryScript inventory;
    ResourceBoxScript resourceBox;
    int stash = 0;

    void Awake()
    {
        if (data && stash == 0)
            stash = data.GetStashData();
        itmImage = GetComponent<Image>();
        inventoryUI = GetComponentInParent<InventoryUI>();
    }

    void Start()
    {
        if (data)
            itmImage.sprite = data.icon;
        else
            itmImage.enabled = false;
    }

    public void ResourceBoxToInventoryClick()
    {
        if (gameObject.activeInHierarchy)
            inventory.AddItems(data, this);
    }

    public void Move()
    {
        resourceBox.AddItems(data, this);
    }

    public void Use()
    {
        if (data)
        {
            data.DoSomething(gameObject, inventory);
            inventoryUI.ActivateInventory(false);
        }

        if (resourceBox)
            resourceBox.OpenClose();
    }

    public void InventoryClick()
    {
        if (gameObject.activeInHierarchy && resourceBox)
        {
            //resourceBox.AddItems(data, this);
            inventoryOptions.GetComponent<InventoryOptions>().SetCurrentItem(this);
            inventoryOptions.transform.position = this.transform.position;
            inventoryOptions.SetActive(true);
        }
        else
            Use();

    }

    public void SetData(ItemsData _data)
    {
        data = _data;
        if (itmImage)
        {
            itmImage.enabled = true;
            if (_data)
                itmImage.sprite = _data.icon;
            else
            {
                itmImage.sprite = null;
                itmImage.enabled = false;
            }
        }
    }

    public ItemsData GetData()
    {
        return data;
    }

    public void SetInventory(InventoryScript _inventory)
    {
        inventory = _inventory;
    }

    public void SetResourceBox(ResourceBoxScript _resourceBox)
    {
        resourceBox = _resourceBox;
    }

    public ResourceBoxScript GetResourceBox()
    {
        return resourceBox;
    }

    public int GetStash()
    {
        return stash;
    }

    public void StashAdd(int amount)
    {
        //Debug.Log(stash);
        stash += amount;
    }

    public void StashSubstraction(int amount)
    {
        stash -= amount;
    }

    public void SetStash(int _stash)
    {
        stash = _stash;
    }

}
