﻿using UnityEngine;

public class DoorScript : MonoBehaviour {
    
    [SerializeField] SceneField sceneLink;
    [SerializeField] uint doorLinkIndex;
    [SerializeField] int level;

    public string GetSceneLinkName()
    {
        return sceneLink;
    }

    public uint GetDoorIndex()
    {
        return doorLinkIndex;
    }

    public void SpawnPlayer()
    {
        PlayerSingleton.singleton.TogglePlayer(true);

        if (SaveManager.singleton.GetGameContinued())
            return;

        Transform playerTransform = PlayerSingleton.singleton.GetPlayer().transform;
        playerTransform.position = this.transform.position;
        playerTransform.rotation = this.transform.rotation;

        playerTransform.GetComponent<PlayerMovement>().GetCameraTransform().forward = -transform.forward;
    }

    public int GetLevel()
    {
        return level;
    }
}
