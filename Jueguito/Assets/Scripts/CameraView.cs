﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraView : MonoBehaviour {

    [SerializeField] Transform cameraTransform;
    [SerializeField] Transform cameraDefaultPosition;
    [SerializeField] Transform cameraClosestPosition;
    [SerializeField] LayerMask layers;
    
	void Update ()
    {
        Ray ray = new Ray(cameraClosestPosition.position, cameraDefaultPosition.position - cameraClosestPosition.position);
        RaycastHit hit = new RaycastHit();

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layers))
        {
            cameraTransform.position = hit.point;
        }
        else
        {
            cameraTransform.position = cameraDefaultPosition.position;
        }

	}
}
