﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCharacterSelector : MonoBehaviour {

    [SerializeField] GameObject[] characters;

	void Awake ()
    {
        for (int i = 0; i < characters.Length; i++)
            characters[i].SetActive(false);

        characters[Random.Range(0, characters.Length)].SetActive(true);
	}
}
