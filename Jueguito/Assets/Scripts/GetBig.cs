﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetBig : MonoBehaviour {

	[SerializeField] Transform transformObject;
    [SerializeField] Vector3 maxSize;
    [SerializeField] float sizeSpeed;

    Vector3 startSize;
    bool resize = true;

    private void Start()
    {
        startSize = transformObject.localScale;
    }

    public void Resize()
    {
        resize = true;
    }

    public void Update()
    {
        if (resize && transform.localScale.magnitude < maxSize.magnitude)
        {
            startSize += sizeSpeed * Vector3.one * Time.deltaTime;
            transform.localScale = startSize;
        }
    }


}
