﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSingleton : MonoBehaviour
{
    public static PlayerSingleton singleton;

    ExperienceScript experience;
    PlayerSoundManager soundManager;

    private bool onDialog = false;

    void Awake()
    {
        DontDestroyOnLoad(this);
        if (singleton != null)
        {
            Destroy(gameObject);
        }
        else
            singleton = this;
    }

    void Start()
    {
        experience = GetComponent<ExperienceScript>();
        soundManager = GetComponent<PlayerSoundManager>();
    }

    public void KillsEnemy()
    {
        experience.AddExperience(1);
    }

    public GameObject GetPlayer()
    {
        return this.gameObject;
    }

    public void PlayChaseMusic()
    {
        soundManager.PlayChaseMusic();
    }

    public void StopChaseMusic()
    {
        soundManager.Stop();
    }

    public void TogglePlayer(bool active)
    {
        soundManager.ResetChase();
        gameObject.SetActive(active);
    }

    public bool OnDialog
    {
        get
        {
            return onDialog;
        }
        set
        {
            onDialog = value;
        }
    }
}
