﻿using UnityEngine.UI;
using UnityEngine;

public class HealthBarScript : MonoBehaviour {

    [SerializeField] Image healthBar;
    
    [SerializeField] float defaultSizeDeltaX;

    public void UpdateHealthBar(float health, float maxHealth)
    {
        Vector2 healthbarSize = new Vector2((defaultSizeDeltaX * health) / maxHealth, healthBar.rectTransform.sizeDelta.y);

        healthBar.rectTransform.sizeDelta = healthbarSize;
    }

}
