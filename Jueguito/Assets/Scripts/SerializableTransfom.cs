﻿using System;
using UnityEngine;

[Serializable]
public class SerializableTransfom {

    public SerializableVector3 position;
    public SerializableQuaternion rotation;

    public SerializableTransfom(Transform transform)
    {
        position = new SerializableVector3(transform.position);
        rotation = new SerializableQuaternion(transform.rotation);
    }
}
