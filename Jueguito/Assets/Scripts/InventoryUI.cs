﻿using UnityEngine;

public class InventoryUI : MonoBehaviour {

    [SerializeField] GameObject inventory;
    [SerializeField] GameObject inventoryOptions;
    [SerializeField] GameObject skillsOptions;

    void Start()
    {
        inventory.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
            ActivateInventory(!inventory.activeSelf);
    }

    public void ActivateInventory(bool value)
    {
        inventory.SetActive(value);

        if (value == false)
        {
            inventoryOptions.SetActive(false);
            skillsOptions.SetActive(false);
        }
    }
}
