﻿using System.Collections.Generic;
using UnityEngine;

public class ResourceBoxManager : MonoBehaviour
{
    public static ResourceBoxManager singleton;

    [SerializeField] List<ResourceBoxScript> rBoxes = new List<ResourceBoxScript>();
    [SerializeField] List<Transform> saveTransforms = new List<Transform>();

    void Awake()
    {
        if (singleton != null)
            Destroy(gameObject);
        else
            singleton = this;
    }

    public List<BoxDataSave> GetBoxes()
    {
        List<BoxDataSave> boxesData = new List<BoxDataSave>();

        foreach (var box in rBoxes)
        {
            List<ItemScript> items = box.GetItems();

            BoxDataSave boxSave = new BoxDataSave();

            foreach (var item in items)
            {
                ItemsData data = item.GetData();

                if (data != null)
                {
                    ItemDataSave itemSave = new ItemDataSave(SaveManager.singleton.GetIndex(data), item.GetStash());

                    boxSave.items.Add(itemSave);
                }
            }
            boxesData.Add(boxSave);
        }
        return boxesData;
    }

    public void SetBoxes(List<BoxDataSave> boxesSave)
    {
        for (int i = 0; i < boxesSave.Count; i++)
        {
            List<ItemSaved> itemScripts = new List<ItemSaved>();
            List<ItemDataSave> itemsSave = boxesSave[i].items;

            for (int j = 0; j < itemsSave.Count; j++)
            {
                ItemSaved itemSaved = new ItemSaved();

                itemSaved.itemData = SaveManager.singleton.GetDataByIndex(itemsSave[j].index);
                itemSaved.stash = itemsSave[j].stash;

                itemScripts.Add(itemSaved);
            }

            rBoxes[i].AddItems(itemScripts);
        }
    }

    public List<SerializableTransfom> GetSaveTransforms()
    {
        List<SerializableTransfom> serializableTransfoms = new List<SerializableTransfom>();

        foreach (var trans in saveTransforms)
            serializableTransfoms.Add(new SerializableTransfom(trans));

        return serializableTransfoms;
    }

    public void SetSaveTransforms(List<SerializableTransfom> serializableTransfoms)
    {
        for (int i = 0; i < serializableTransfoms.Count; i++)
        {
            saveTransforms[i].position = serializableTransfoms[i].position.ToVector3();
            saveTransforms[i].rotation = serializableTransfoms[i].rotation.ToQuaternion();
        }
    }
}
