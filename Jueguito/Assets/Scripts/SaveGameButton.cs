﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SaveGameButton : MonoBehaviour {

    [SerializeField] GameObject savedFeedback;

    Button button;

    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(SaveGame);
    }

    void SaveGame()
    {
        SaveManager.singleton.Save();
        savedFeedback.SetActive(true);
    }
}
