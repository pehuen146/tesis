﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackScript : MonoBehaviour {

    [SerializeField] GameObject hitBox;

    void Attack()
    {
        Debug.Log("Attack");

        if (hitBox)
            hitBox.SetActive(true);
    }

    void EndAttack()
    {
        Debug.Log("EndAttack");

        if (hitBox)
            hitBox.SetActive(false);
    }
}
