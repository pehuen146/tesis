﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    [SerializeField] EnemyDT dt;
    [SerializeField] GameObject[] bloodParticles;

    EnemySoundManager soundManager;
    EnemyIA eIA;
    Animator animator;

    float health;

    void Bleed()
    {
        bloodParticles[Random.Range(0, bloodParticles.Length)].SetActive(true);
    }

    void Start()
    {
        health = dt.GetHealth();
        soundManager = GetComponent<EnemySoundManager>();
        eIA = GetComponent<EnemyIA>();
        animator = GetComponentInChildren<Animator>();
    }

    public float GetHealth()
    {
        return health;
    }

    public void TakeDamage(float damage)
    {
        //PlayerSingleton.singleton.KillsEnemy();

        health -= damage;

        Debug.Log(eIA);

        if (IsAlive())
        {
            Vector3 playerPos = PlayerSingleton.singleton.GetPlayer().transform.position;
            eIA.HearSound(playerPos);

            eIA.Stun();
            Bleed();
        }
        else
        {
            health = 0;
            Invoke("Death", 3);
            soundManager.PlayDeathSound();
            animator.applyRootMotion = true;
            animator.SetTrigger("Death");
            //animator.SetBool("Dead", true);
        }
    }

    public bool IsAlive()
    {
        return health > 0;
    }

    void Death()
    {
        gameObject.SetActive(false);
    }
}
