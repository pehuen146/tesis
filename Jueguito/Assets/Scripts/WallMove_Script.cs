﻿using UnityEngine;
using UnityEngine.Events;

public class WallMove_Script : MonoBehaviour
{
    [SerializeField] string playerTag;
    [SerializeField] GameObject fenceGO;
    [SerializeField] DialogueTrigger noteDialogue;
    [SerializeField] DialogueTrigger weaponDialogue;
    [SerializeField] UnityEvent fenceOpenEvent;

    Animator animator;

    private void Start()
    {
        animator = fenceGO.GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(playerTag))
        {
            InventoryScript inventoryScript = other.gameObject.GetComponent<InventoryScript>();
            ItemScript currentWeapon = other.gameObject.GetComponent<Shot>().GetCurrentWeapon();

            if (inventoryScript.SearchTypeItem(ItemsData.Item.Note) && currentWeapon.GetData())
            {
                animator.SetTrigger("Open");
                gameObject.SetActive(false);
                fenceOpenEvent.Invoke();
            }
            else if (!inventoryScript.SearchTypeItem(ItemsData.Item.Note))
                noteDialogue.TriggerDialogue();
            else
                weaponDialogue.TriggerDialogue();
        }
    }

}
