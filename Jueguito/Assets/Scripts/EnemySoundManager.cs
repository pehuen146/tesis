﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySoundManager : MonoBehaviour {


    [SerializeField] AudioClip attackSound;
    [SerializeField] AudioClip canSeePlayerSound;
    [SerializeField] AudioClip deathSound;

    AudioSource audioSource;

    void Start ()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayAttackSound()
    {
        audioSource.PlayOneShot(attackSound);
    }

    public void PlayCanSeePlayerSound()
    {
        audioSource.PlayOneShot(canSeePlayerSound);
    }

    public void PlayDeathSound()
    {
        audioSource.PlayOneShot(deathSound);
    }
}
