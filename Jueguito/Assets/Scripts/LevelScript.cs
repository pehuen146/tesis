﻿using UnityEngine;
using UnityEngine.Events;

public class LevelScript : MonoBehaviour {

    [System.Serializable]
    public class ToggleEvent : UnityEvent<bool> { }

    [SerializeField] int limitLevel;
    [SerializeField] ToggleEvent belowLevel;
    [SerializeField] ToggleEvent aboveLevel;

    LevelManager levelManager;

	void Start ()
    {
        levelManager = PlayerSingleton.singleton.GetComponent<LevelManager>();
        int currentLevel = levelManager.GetCurrentLevel();

        if (currentLevel < limitLevel)
        {
            belowLevel.Invoke(true);
            aboveLevel.Invoke(false);
        }
        else
        {
            aboveLevel.Invoke(true);
            belowLevel.Invoke(false);
        }
	}


}
