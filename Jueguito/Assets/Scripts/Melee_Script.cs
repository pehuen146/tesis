﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee_Script : MonoBehaviour
{
    [SerializeField] GameObject meleeCollider;
    //[SerializeField] ItemScript currentMeleeWeapon;
    [SerializeField] MeleeWeaponsData fistData;
    //[SerializeField] InventoryScript inventory;
    [SerializeField] float additionalMeleeDamagePerLevel;

    Timer timer;
    ExperienceScript experience;
    Animator animator;
    bool hasStashed = false;
    float additionalMeleeDamage = 0;

    bool punching = false;

    private void Start()
    {
        experience = GetComponent<ExperienceScript>();
        timer = new Timer();
        meleeCollider.SetActive(false);
        //fistData = (MeleeWeaponsData)currentMeleeWeapon.GetData();
        animator = GetComponent<Animator>();
        timer.SetActive(true);
        timer.SetTime(fistData.GetCooldown(), false);
    }

    private void Update()
    {
        if (timer.TimeUp())
        {
            meleeCollider.SetActive(false);

            if (Input.GetKeyDown(KeyCode.V))
                if (fistData)
                {

                    punching = true;
                    StartCoroutine(punchingTimer());

                    switch (Random.Range(0, 2))
                    {
                        case 0:
                            animator.SetTrigger("Punch");
                            break;
                        case 1:
                            animator.SetTrigger("Punch2");
                            break;
                        default:
                            Debug.Log("Error de animacion");
                            break;
                    }

                    meleeCollider.SetActive(true);
                    Debug.Log(meleeCollider);
                    timer.Reset();
                }
        }

        timer.Update();
    }

    public float GetMeleeDamage()
    {
        additionalMeleeDamage = experience.GetCurrentLevel(SkillType.Strength) * additionalMeleeDamagePerLevel;
        return fistData.GetDamage() + additionalMeleeDamage;
    }

    public bool IsPunching()
    {
        return punching;
    }

    IEnumerator punchingTimer()
    {        
        yield return new WaitForSeconds(fistData.GetCooldown());
        punching = false;
    }
}
