﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{

    [SerializeField] EnemyDT dt;
    [SerializeField] string playerTag;
    [SerializeField] Animator animator;

    const float collisionTime = .5f;

    Timer attackTimer;
    EnemyHealth health;
    EnemySoundManager soundManager;

    private void OnEnable()
    {
        attackTimer.Reset();
    }

    void Awake()
    {
        attackTimer = new Timer();
        attackTimer.SetTime(collisionTime, true);
        soundManager = GetComponentInParent<EnemySoundManager>();
        health = GetComponentInParent<EnemyHealth>();
    }

    void Update()
    {
        attackTimer.Update();

        if (attackTimer.TimeUp())
            gameObject.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(playerTag) && health.IsAlive())
        {
            other.GetComponent<Health>().TakeDamage(dt.GetDamage());
            gameObject.SetActive(false);
        }
    }
}
