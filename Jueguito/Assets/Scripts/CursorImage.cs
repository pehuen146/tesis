﻿using UnityEngine.EventSystems;
using UnityEngine;

public class CursorImage : MonoBehaviour {

    [SerializeField]
    Texture2D image;

    //bool onUI = false;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
    }

    private void Update()
    {

        Cursor.SetCursor(image, new Vector2((image.width / 2), (image.height / 2)), CursorMode.Auto);

        /*if (EventSystem.current && onUI != EventSystem.current.IsPointerOverGameObject())
        {
            onUI = EventSystem.current.IsPointerOverGameObject();

            if (!EventSystem.current.IsPointerOverGameObject())
                Cursor.SetCursor(image, new Vector2((image.width / 2), (image.height / 2)), CursorMode.Auto);
            else
                Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }*/

    }

    private void OnDestroy()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }
}
