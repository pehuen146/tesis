using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class SaveManager : MonoBehaviour
{
    const int emptySlotIndex = 500;
    public static SaveManager singleton;

    [SerializeField] List<Transform> positionSaves;
    [SerializeField] ItemsData[] dataObjects;


    bool gameContinued = false;

    DataSave dataSave = null;

    Vector3 playerPosition;

    void Awake()
    {
        DontDestroyOnLoad(this);
        if (singleton != null)
            Destroy(gameObject);
        else
            singleton = this;
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(GetPath());

        if (dataSave == null)
            dataSave = new DataSave();

        string currentScene = SceneManager.GetActiveScene().name;
        dataSave.sceneName = currentScene;

        Transform playerTransform = PlayerSingleton.singleton.transform;

        dataSave.playerPosition = new SerializableVector3(playerTransform.position);
        dataSave.playerRotation = new SerializableQuaternion(playerTransform.GetComponent<PlayerMovement>().GetCameraTransform().rotation);
        dataSave.playerLevel = PlayerSingleton.singleton.GetComponent<LevelManager>().GetCurrentLevel();
        dataSave.playerHealth = playerTransform.GetComponent<Health>().GetHealth();

        Shot shotScript = playerTransform.GetComponent<Shot>();

        if (shotScript.GetData() != null)
            dataSave.equipedWeaponIndex = GetIndex(shotScript.GetData());
        else
            dataSave.equipedWeaponIndex = emptySlotIndex;


        dataSave.bulletsEquiped = shotScript.GetCharger();

        Debug.Log("Guardado " + dataSave.playerHealth);

        SceneDataSave scene = FindScene(currentScene);

        List<ItemScript> inventoryItems = playerTransform.GetComponent<InventoryScript>().GetInventoryItems();
        List<ItemDataSave> savedItems = new List<ItemDataSave>();

        ExperienceScript experienceScript = PlayerSingleton.singleton.GetComponent<ExperienceScript>();

        dataSave.skills = experienceScript.GetSkills();
        dataSave.experience = experienceScript.GetExperience();
        dataSave.experiencePoints = experienceScript.GetExperiencePoints();

        dataSave.inventoryItems.Clear();
        for (int i = 0; i < inventoryItems.Count; i++)
        {
            ItemScript item = inventoryItems[i];
            ItemDataSave newItemSave = new ItemDataSave(GetIndex(item.GetData()), item.GetStash());

            dataSave.inventoryItems.Add(newItemSave);
        }

        if (scene == null)
        {
            scene = new SceneDataSave();
            scene.sceneName = currentScene;
            dataSave.scenes.Add(scene);
        }

        scene.boxes = ResourceBoxManager.singleton.GetBoxes();
        scene.saveTransforms = ResourceBoxManager.singleton.GetSaveTransforms();

        dataSave.flaslightActivated = playerTransform.GetComponent<FlashlightActivator>().isFlashlightActivated();

        bf.Serialize(file, dataSave);
        file.Close();
    }    

    public void Load()
    {
        if (File.Exists(GetPath()))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(GetPath(), FileMode.Open);

            dataSave = (DataSave)bf.Deserialize(file);
            file.Close();

            if (gameContinued)
            {
                Transform playerTransform = PlayerSingleton.singleton.transform;

                playerTransform.position = dataSave.playerPosition.ToVector3();
                playerTransform.GetComponent<PlayerMovement>().GetCameraTransform().rotation = dataSave.playerRotation.ToQuaternion();
                playerTransform.GetComponent<LevelManager>().SetLevel(dataSave.playerLevel);
                playerTransform.GetComponent<Health>().SetHealth(dataSave.playerHealth);

                Debug.Log("Leido " + dataSave.playerHealth);

                if (dataSave.flaslightActivated)
                    playerTransform.GetComponent<FlashlightActivator>().ActivateFlashlight();

                InventoryScript inventory = playerTransform.GetComponent<InventoryScript>();
                Shot shotScript = playerTransform.GetComponent<Shot>();
                inventory.ClearInventory();

                for (int i = 0; i < dataSave.inventoryItems.Count; i++)
                {
                    ItemDataSave itemSaved = dataSave.inventoryItems[i];

                    inventory.AddItems(dataObjects[itemSaved.index], itemSaved.stash);
                }

                ExperienceScript experienceScript = PlayerSingleton.singleton.GetComponent<ExperienceScript>();

                experienceScript.SetSkills(dataSave.skills);
                experienceScript.SetExperiencePoints(dataSave.experiencePoints);
                experienceScript.SetExperience(dataSave.experience);

                if (dataSave.equipedWeaponIndex != emptySlotIndex)
                {
                    WeaponsData weaponsData = (WeaponsData)GetDataByIndex(dataSave.equipedWeaponIndex);
                    shotScript.SetAmmo(0);
                    inventory.AddItems(weaponsData.GetBulletsData(), dataSave.bulletsEquiped);
                    weaponsData.DoSomething(PlayerSingleton.singleton.GetPlayer(), inventory);
                }
            }

            SceneDataSave scene = FindScene(SceneManager.GetActiveScene().name);

            if (scene != null)
            {
                ResourceBoxManager.singleton.SetBoxes(scene.boxes);

                if (scene.saveTransforms.Count > 0)
                    ResourceBoxManager.singleton.SetSaveTransforms(scene.saveTransforms);
            }
        }
    }

    public void DeleteSave()
    {
        if (File.Exists(GetPath()))
            File.Delete(GetPath());

        dataSave = null;
    }

    string GetPath()
    {
        return Application.persistentDataPath + "/data";
    }

    SceneDataSave FindScene(string sceneName)
    {
        foreach (var scene in dataSave.scenes)
            if (scene.sceneName == sceneName)
                return scene;
        
        return null;
    }

    public int GetIndex(ItemsData item)
    {
        for (int i = 0; i < dataObjects.Length; i++)
            if (dataObjects[i] == item)
                return i;

        return 0;
    }

    public ItemsData GetDataByIndex(int index)
    {
        return dataObjects[index];
    }

    public bool CheckIfFileExist()
    {
        bool exists = File.Exists(GetPath());

        return exists;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Load();
    }

    public string GetSavedScene()
    {
        if (dataSave != null)
            return dataSave.sceneName;

        return null;
    }

    public void SetPlayerPosition(Vector3 position)
    {
        playerPosition = position;
    }

    public void SetGameContinued(bool value)
    {
        gameContinued = value;
    }

    public bool GetGameContinued()
    {
        return gameContinued;
    }
}
