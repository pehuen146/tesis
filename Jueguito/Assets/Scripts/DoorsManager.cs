﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorsManager : MonoBehaviour
{
    public static DoorsManager singleton;

    [SerializeField] DoorScript[] doors;
    
    void Awake()
    {
        if (singleton != null)
            Destroy(gameObject);
        else
            singleton = this;
    }

    void Start()
    {
        if (SaveManager.singleton.GetGameContinued())
            SaveManager.singleton.SetGameContinued(false);
        else
            doors[SceneManagement.singleton.GetDoorIndex()].SpawnPlayer();
    }

    public DoorScript GetDoor(uint doorIndex)
    {
        if (doorIndex < doors.Length)
            return doors[doorIndex];
        else
        {
            Debug.LogError("Puerta no encontrada");
            return null;
        }
    }
}
