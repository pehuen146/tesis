﻿using UnityEngine;
using UnityEngine.UI;

public class ExperienceScript : MonoBehaviour
{
    [SerializeField] Image experienceBar;
    [SerializeField] Text xpPointsUI;
    [SerializeField] float maxExperience; 
    [SerializeField] int maxExperiencePoints;
    [SerializeField] int maxSkillLevel;
    
    InventoryScript inventory;
    Shot shot;
    float currentExperience = 0;
    int experiencePoints = 0;

    int[] skills = new int[(int)SkillType.SKILLS_AMOUNT];

    void Start()
    {
        inventory = GetComponent<InventoryScript>();
        shot = GetComponent<Shot>();
        UpdateBar();
    }

    public void UpdateBar()
    {
        xpPointsUI.text = experiencePoints.ToString();
        experienceBar.rectTransform.localScale = new Vector3(currentExperience / maxExperience, 1, 1);
    }

    public void AddExperience(float experience)
    {
        if (currentExperience + experience < maxExperience)
            currentExperience += experience;
        else
        {
            currentExperience = 0;
            if (experiencePoints < maxExperiencePoints)
                experiencePoints++;
        }
        UpdateBar();
    }

    public void UseExperiencePoint(SkillType skill)
    {
        if (experiencePoints <= 0)
            return;

        int skillIndex = (int)skill;
        int skillLevel = skills[skillIndex];

        if (skillLevel < maxSkillLevel)
        {
            skillLevel++;
            experiencePoints--;
            UpdateBar();

            switch (skill)
            {
                case SkillType.BackPack:
                    inventory.SetBackPackLevel(skillLevel);
                    break;
            }
        }
        else if (skillLevel > maxSkillLevel)
            skillLevel = maxSkillLevel;

        skills[skillIndex] = skillLevel;
    }

    public int GetCurrentLevel(SkillType skill)
    {
        return skills[(int)skill];
    }

    public int GetMaxSkillLevel()
    {
        return maxSkillLevel;
    }

    public int[] GetSkills()
    {
        return skills;
    }

    public float GetExperience()
    {
        return currentExperience;
    }

    public void SetExperience(float _experience)
    {
        currentExperience = _experience;
    }

    public int GetExperiencePoints()
    {
        return experiencePoints;
    }

    public void SetExperiencePoints(int _experiencePoints)
    {
        experiencePoints = _experiencePoints;
    }

    public void SetSkills(int[] _skills)
    {
        for (int i = 0; i < _skills.Length; i++)
            skills[i] = _skills[i];
    }
}

public enum SkillType
{
    Resistance, Strength, Agility, ReloadVelocity, BackPack, SKILLS_AMOUNT
};
