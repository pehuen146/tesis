﻿using UnityEngine;
using UnityEngine.UI;

public class ExitOnDialog : MonoBehaviour {

    Button button;

    private void Awake()
    {
        button = GetComponent<Button>();

        button.onClick.AddListener(OnDialogExit);
    }

    void OnDialogExit()
    {
        PlayerSingleton.singleton.OnDialog = false;
    }
}
