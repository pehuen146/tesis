﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : MonoBehaviour
{
    [SerializeField] GameObject pauseCanvas;

    float previousTimescale = 1;

    bool paused = false;

    void Start()
    {
        pauseCanvas.SetActive(paused);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            ChangePauseState();
    }

    public void ChangePauseState()
    {
        /*if (Time.timeScale == 0)
            paused = true;
        else
            paused = false;*/

        paused = !paused;

        EnemyIA[] enemyIAs = FindObjectsOfType<EnemyIA>();

        foreach (var item in enemyIAs)
            item.enabled = !paused;

        if (paused)
        {
            previousTimescale = Time.timeScale;
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = previousTimescale;
        }
        

        pauseCanvas.SetActive(paused);
    }
}
