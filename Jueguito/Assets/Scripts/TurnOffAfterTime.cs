﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOffAfterTime : MonoBehaviour {

    [SerializeField] float time;

    private void OnEnable()
    {
        StartCoroutine(TurnOffFeedback());
    }

    IEnumerator TurnOffFeedback()
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }
}
