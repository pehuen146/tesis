﻿using UnityEngine.UI;
using UnityEngine;

public class StashUI_Script : MonoBehaviour {

    [SerializeField] Text stashUI;
    [SerializeField] ItemScript item;

    void Update()
    {
        stashUI.text = "x" + item.GetStash();
    }

}
