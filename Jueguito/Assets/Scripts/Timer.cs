﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer{

    float timer = 0;
    float additionalTime = 0;
    float time;
    bool active = true;

    
    public void SetTime(float _time, bool reset)
    {
        time = _time;

        if (reset)
            Reset();
    }

    public void Reset()
    {
        timer = time + additionalTime;
    }

    public void SetAdditionalTime(float addTime)
    {
        additionalTime = addTime;
    }

    public bool TimeUp()
    {
        if (timer <= 0)
            return true;
        else
            return false;
    }

	public void Update ()
    {
        if (active && timer > 0)
            timer -= Time.deltaTime;
    }

    public void SetActive(bool value)
    {
        active = value;
    }

    public bool GetActive()
    {
        return active;
    }
}
