﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon_Script : MonoBehaviour
{
    [SerializeField] float horizontalSpeed;
    [SerializeField] GameObject cannonCanvas;
    [SerializeField] Transform rotationObject;
    [SerializeField] string bossTag;
    [SerializeField] float shotCooldown;
    [SerializeField] float shotParticlesTime = 0.15f;
    [SerializeField] float damage;
    [SerializeField] float health = 300;
    [SerializeField] GameObject shotParticles;
    [SerializeField] GameObject triggerEnterGO;
    [SerializeField] GameObject destroyParticlesGO;
    [SerializeField] GameObject healthBarGO;

    Camera cannonCamera;
    GameObject player;
    AudioSource audioSource;
    GetSpawnTransform spawnPoint;
    Timer shotTimer;
    Timer particlesTimer;
    HealthBarScript healthBar;
    bool isAlive = true;

    float maxHealth;

    float h = 0;

    private void Awake()
    {
        cannonCamera = GetComponentInChildren<Camera>();
        spawnPoint = GetComponent<GetSpawnTransform>();
        audioSource = GetComponent<AudioSource>();
        healthBar = GetComponent<HealthBarScript>();
        shotTimer = new Timer();
        particlesTimer = new Timer();

        cannonCamera.enabled = false;
        shotTimer.SetTime(shotCooldown, false);
        shotTimer.SetActive(true);
        shotParticles.SetActive(false);
        particlesTimer.SetActive(true);
        particlesTimer.SetTime(shotParticlesTime, false);

        maxHealth = health;
        isAlive = true;
    }

    private void Start()
    {
        healthBar.UpdateHealthBar(health, maxHealth);
    }

    private void FixedUpdate()
    {
        shotTimer.Update();
        if (player)
        {
            MoveCamera();
            Shot();
            if (Input.GetKeyDown(KeyCode.E))
                LeaveCanon();
        }
    }

    public void EnterCanon(GameObject _player)
    {
        player = _player;
        player.SetActive(false);
        cannonCamera.enabled = true;
        healthBarGO.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        cannonCanvas.SetActive(true);
    }

    public void LeaveCanon()
    {
        if (player)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            healthBarGO.SetActive(true);
            cannonCanvas.SetActive(false);
            cannonCamera.enabled = false;
            shotParticles.SetActive(false);
            player.GetComponent<PlayerMovement>().ResistanceRefill();
            player.transform.position = spawnPoint.GetSpawnPoint();
            player.SetActive(true);
            player = null;
        }
    }

    void Shot()
    {
        particlesTimer.Update();
        if (Input.GetMouseButtonDown(0) && shotTimer.TimeUp())
        {
            RaycastHit hit;
            audioSource.Play();

            if (Physics.Raycast(cannonCamera.transform.position, cannonCamera.transform.forward, out hit, Mathf.Infinity))
            {
                if (hit.transform.CompareTag(bossTag))
                    hit.collider.gameObject.GetComponent<EnemyBossHealth>().TakeDamage(damage);

                Debug.Log("SHOT");
            }
            shotTimer.Reset();
            particlesTimer.Reset();
        }

        if (particlesTimer.TimeUp())
            shotParticles.SetActive(false);
        else
            shotParticles.SetActive(true);
    }

    public void TakeDamage(float damage)
    {
        health -= damage;

        healthBar.UpdateHealthBar(health, maxHealth);

        if (health <= 0)
            CannonDestroy();
    }

    public void CannonDestroy()
    {
        if (player)
            LeaveCanon();

        health = 0;
        healthBar.UpdateHealthBar(health, maxHealth);
        isAlive = false;
        triggerEnterGO.SetActive(false);
        destroyParticlesGO.SetActive(true);
    }

    void MoveCamera()
    {
        h = horizontalSpeed * Input.GetAxis("Mouse X");

        rotationObject.Rotate(0, h, 0);
    }

    public bool GetIsAlive()
    {
        return isAlive;
    }
}
