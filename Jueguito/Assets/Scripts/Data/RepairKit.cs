﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("Game/Data/Repair Kit"))]
public class RepairKit : ItemsData
{
    [SerializeField] float repairAmount;
    [SerializeField] string nameOfDialogue;

    DialogueTrigger dialogue;

    public override void DoSomething(GameObject other, InventoryScript inventory)
    {
        CarInput ci = other.GetComponentInParent<EnterObject_Script>().GetCarInput();

        if (ci)
        {
            ItemScript itemScript = other.GetComponent<ItemScript>();

            if (audioClip)
            {
                AudioSource audioSource = other.GetComponentInParent<AudioSource>();
                audioSource.PlayOneShot(audioClip);
            }

            ci.RepairVehicle(repairAmount);
            inventory.WeightSubstraction(weight);
            itemScript.StashSubstraction(1);
            if (itemScript.GetStash() <= 0)
            {
                other.SetActive(false);
                other.GetComponent<ItemScript>().SetData(null);
            }
        }
        else
        {
            dialogue = FindObjectOfType<Dialogues>().GetDialogue(nameOfDialogue);
            dialogue.TriggerDialogue();
        }
    }
}