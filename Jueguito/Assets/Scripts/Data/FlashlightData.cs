﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("Game/Data/Flashlight"))]
public class FlashlightData : ItemsData {

    public override void DoSomething(GameObject other, InventoryScript inventory)
    {
        ItemScript itemScript = other.GetComponent<ItemScript>();
        other.GetComponentInParent<FlashlightActivator>().ActivateFlashlight();

        itemScript.StashSubstraction(1);
        if (itemScript.GetStash() <= 0)
        {
            other.SetActive(false);
            other.GetComponent<ItemScript>().SetData(null);
        }
    }

}
