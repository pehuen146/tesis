﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("Game/Data/Bullets"))]
public class BulletsData : ItemsData {

    [SerializeField] float speed;
    [SerializeField] float maxTime;
    [SerializeField] float damage;

    public float GetSpeed()
    {
        return speed;
    }

    public float GetMaxTime()
    {
        return maxTime;
    }

    public float GetDamage()
    {
        return damage;
    }

}