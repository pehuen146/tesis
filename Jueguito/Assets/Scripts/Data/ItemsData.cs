﻿using UnityEngine;

[CreateAssetMenu(menuName = ("Game/Data/Items"))]
public class ItemsData : ScriptableObject
{
    public enum Item { Weapon, Bullets, Healing, Note, Melee, Battery, Map, Chemic, Flashlight, RepairKit };

    public string itemName;
    public int index;
    public Sprite icon;
    public float weight;
    public Item item;
    [SerializeField] int stash;
    [SerializeField] protected AudioClip audioClip;

    public int GetStashData()
    {
        return stash;
    }

    public void SetStashData(int value)
    {
        stash = value;
    }

    public virtual void DoSomething(GameObject other, InventoryScript inventory) { }

    public virtual void LevelUP(GameObject other) { }
}