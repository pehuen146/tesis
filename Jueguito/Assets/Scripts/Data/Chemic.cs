﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("Game/Data/Chemic"))]
public class Chemic : ItemsData
{
    [SerializeField] int level;

    public override void DoSomething(GameObject other, InventoryScript inventory)
    {
    }

    public override void LevelUP(GameObject other)
    {
        if (audioClip)
        {
            AudioSource audioSource = other.GetComponentInParent<AudioSource>();
            audioSource.PlayOneShot(audioClip);
        }

        PlayerSingleton.singleton.GetComponent<LevelManager>().SetLevel(level);

        //Debug.Log("nivel" + PlayerSingleton.singleton.GetComponent<LevelManager>().GetCurrentLevel());
    }

}
