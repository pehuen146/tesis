﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("Game/Data/Enemy"))]
public class EnemyData : ScriptableObject {

    public string enemyName;
    public float damage;
    public float speed;
    public float health;
    public float EnemyVision; //Distance vision
    public float fOV; // Field of vision
    public float attackDistance;
    public float minimumDistancePatrolPoint = 0.5f;
    public float waitTime;
    public float attackResetTime;
}
