﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("Game/Data/Melee Weapons"))]
public class MeleeWeaponsData : ItemsData
{
    public enum MeleeType { Fist, Other};

    [SerializeField] MeleeType meleeType;
    [SerializeField] float cooldown;
    [SerializeField] float BreakResistance;
    [SerializeField] float damage;
    [SerializeField] AudioClip[] meleeSounds;

    /*public override void DoSomething(GameObject other, InventoryScript inventory)
    {
        Melee_Script meleeScript = other.GetComponentInParent<Melee_Script>();
        ItemScript itemScript = other.GetComponent<ItemScript>();
        if (meleeScript.GetMeleeWeaponsData())
        {
            if (meleeScript.GetMeleeWeaponsData().index != this.index)
                meleeScript.SetData(this, itemScript);
        }
        else
            meleeScript.SetData(this, itemScript);
    }*/

    public float GetDamage()
    {
        return damage;
    }

    public float GetCooldown()
    {
        return cooldown;
    }

    public MeleeType GetMeleeType()
    {
        return meleeType;
    }

    public AudioClip[] GetMeleeSounds()
    {
        return meleeSounds;
    }

}
