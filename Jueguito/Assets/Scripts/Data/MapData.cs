﻿using UnityEngine;

[CreateAssetMenu(menuName = ("Game/Data/Map"))]
public class MapData : ItemsData
{

    [SerializeField] Sprite mapImage;


    public override void DoSomething(GameObject other, InventoryScript inventory)
    {
        if (audioClip)
        {
            AudioSource audioSource = other.GetComponentInParent<AudioSource>();
            audioSource.PlayOneShot(audioClip);
        }

        /* NoteScript noteScript = other.GetComponentInParent<NoteScript>();
         noteScript.OpenNote();
         noteScript.SetText(noteText);
         noteScript.SetKeyAddress(keyAddress);
         PlayerSingleton.singleton.GetComponent<LevelManager>().SetLevel(level);*/

        Map mapScript = other.GetComponentInParent<Map>();

        mapScript.ShowMap(mapImage);

        //Debug.Log("nivel" + level);
    }
}
