﻿using UnityEngine;

[CreateAssetMenu(menuName = ("Game/Data/Healing Items"))]
public class HealingItemsData : ItemsData
{
    public float healthAmount;

    public override void DoSomething(GameObject other, InventoryScript inventory)
    {
        Health health = other.GetComponentInParent<Health>();
        ItemScript itemScript = other.GetComponent<ItemScript>();

        if (audioClip)
        {
            AudioSource audioSource = other.GetComponentInParent<AudioSource>();
            audioSource.PlayOneShot(audioClip);
        }

        health.Heal(healthAmount);
        inventory.WeightSubstraction(weight);
        itemScript.StashSubstraction(1);
        if (itemScript.GetStash() <= 0)
        {
            other.SetActive(false);
            other.GetComponent<ItemScript>().SetData(null);
        }
    }
}
