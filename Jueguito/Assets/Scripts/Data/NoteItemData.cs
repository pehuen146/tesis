﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = ("Game/Data/Note Item"))]
public class NoteItemData : ItemsData
{
    [SerializeField] string noteText;
    [SerializeField] int level;
    [SerializeField] string keyAddress;
    

    public override void DoSomething(GameObject other, InventoryScript inventory)
    {
        if (audioClip)
        {
            AudioSource audioSource = other.GetComponentInParent<AudioSource>();
            audioSource.PlayOneShot(audioClip);
        }

        NoteScript noteScript = other.GetComponentInParent<NoteScript>();
        noteScript.OpenNote();
        noteScript.SetText(noteText);
        noteScript.SetKeyAddress(keyAddress);
        PlayerSingleton.singleton.GetComponent<LevelManager>().SetLevel(level);

        PlayerSingleton.singleton.OnDialog = true;
    }

    public override void LevelUP(GameObject other)
    {
        /*if (audioClip)
        {
            AudioSource audioSource = other.GetComponentInParent<AudioSource>();
            audioSource.PlayOneShot(audioClip);
        }

        NoteScript noteScript = other.GetComponentInChildren<NoteScript>();
        noteScript.OpenNote();
        noteScript.SetText(noteText);
        noteScript.SetKeyAddress(keyAddress);
        PlayerSingleton.singleton.GetComponent<LevelManager>().SetLevel(level);

        PlayerSingleton.singleton.OnDialog = true;*/

        InventoryScript inventoryScript = other.GetComponentInParent<InventoryScript>();
        inventoryScript.SearchNote(this).Use();

    }

}
