using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("Game/Data/Weapons"))]
public class WeaponsData : ItemsData
{
    public enum WeaponType { Pistol, Rifle, Shotgun, Knife, Empty };

    public float shotCooldown;
    public float reloadTime;
    public int chargerCapacity;
    public WeaponType weaponType;
    [SerializeField] int bulletIndex;
    [SerializeField] AudioClip reloadSound;
    [SerializeField] AudioClip[] gunShotSounds;
    [SerializeField] BulletsData bulletsData;

    ItemScript ammo;

    public override void DoSomething(GameObject other, InventoryScript inventory)
    {
        if (audioClip)
        {
            AudioSource audioSource = other.GetComponentInParent<AudioSource>();
            audioSource.PlayOneShot(audioClip);
        }

        List<ItemScript> invItms = inventory.GetInventoryItems();
        Shot shotScript = other.GetComponentInParent<Shot>();

        for (int i = 0; i < invItms.Capacity; i++)
            if (invItms[i].GetData())
                if (invItms[i].GetData().index == bulletIndex)
                    ammo = invItms[i];

        if (shotScript.GetData())
        {
            if (shotScript.GetData().index != this.index)
                shotScript.SetData(this);
        }
        else
            shotScript.SetData(this);
    }

    public ItemScript GetAmmo()
    {
        return ammo;
    }

    public void SetAmmo(ItemScript _ammo)
    {
        ammo = _ammo;
    }

    public AudioClip GetReloadSound()
    {
        return reloadSound;
    }

    public AudioClip[] GetGunShotSounds()
    {
        return gunShotSounds;
    }

    public int GetBulletIndex()
    {
        return bulletIndex;
    }

    public BulletsData GetBulletsData()
    {
        return bulletsData;
    }

}
