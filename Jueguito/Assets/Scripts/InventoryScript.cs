﻿using System.Collections.Generic;
using UnityEngine;

public class InventoryScript : MonoBehaviour {

    [SerializeField] List<ItemScript> inventoryItems;
    [SerializeField] int maxWeight;
    [SerializeField] int additionalMaxWeightPerLevel;
    [SerializeField] GameObject inventoryOptions;

    int additionalWeight = 0;
    
    Shot shotScript;
    PlayerSoundManager soundManager;

    void Start()
    {
        shotScript = GetComponentInParent<Shot>();
        soundManager = GetComponent<PlayerSoundManager>();
    }

    float currentWeight = 0;

    public void AddItems(ItemsData data, ItemScript item)
    {
        if (soundManager)
            soundManager.PlayObjectMove();

        for (int i = 0; i < inventoryItems.Capacity; i++)
        {
            if (inventoryItems[i].GetData() && currentWeight < maxWeight + additionalWeight)
            {
                if (inventoryItems[i].GetData().index == data.index && data.index != 3)
                {
                    inventoryItems[i].SetInventory(this);
                    currentWeight += data.weight;
                    item.gameObject.SetActive(false);
                    inventoryItems[i].StashAdd(item.GetStash());

                    if (data.item == ItemsData.Item.Bullets)
                        shotScript.UpdateBullets();

                    if (data.item == ItemsData.Item.Chemic || data.item == ItemsData.Item.Note)
                        data.LevelUP(gameObject);

                    item.SetData(null);
                    return;
                }
            }
        }

        for (int i = 0; i < inventoryItems.Capacity; i++)
        {
            if (!inventoryItems[i].GetData() && currentWeight < maxWeight + additionalWeight)
            {
                inventoryItems[i].SetData(data);
                inventoryItems[i].SetInventory(this);
                currentWeight += data.weight;
                item.gameObject.SetActive(false);
                inventoryItems[i].gameObject.SetActive(true);
                inventoryItems[i].SetStash(item.GetStash());

                if (data.item == ItemsData.Item.Bullets)
                    shotScript.UpdateBullets();

                if (data.item == ItemsData.Item.Chemic || data.item == ItemsData.Item.Note)
                    data.LevelUP(gameObject);

                item.SetData(null);
                return;
            }
        }
    }

    public void AddItems(ItemsData data, int stash)
    {
        if (soundManager)
            soundManager.PlayObjectMove();

        if (stash > 0)
        {
            for (int i = 0; i < inventoryItems.Capacity; i++)
            {
                if (inventoryItems[i].GetData() && currentWeight < maxWeight + additionalWeight)
                {
                    if (inventoryItems[i].GetData().index == data.index && data.index != 3)
                    {
                        inventoryItems[i].SetInventory(this);
                        inventoryItems[i].StashAdd(stash);
                        currentWeight += data.weight;
                        return;
                    }
                }
            }

            for (int i = 0; i < inventoryItems.Capacity; i++)
            {
                if (!inventoryItems[i].GetData() && currentWeight < maxWeight + additionalWeight)
                {
                    inventoryItems[i].SetData(data);
                    inventoryItems[i].SetInventory(this);
                    currentWeight += data.weight;
                    inventoryItems[i].SetStash(stash);
                    inventoryItems[i].gameObject.SetActive(true);
                    return;
                }
            }
        }
    }

    public void ClearInventory()
    {
        for (int i = 0; i < inventoryItems.Count; i++)
        {
            inventoryItems[i].SetData(null);
            inventoryItems[i].SetStash(0);
        }
    }

    public void WeightSubstraction(float amount)
    {
        currentWeight -= amount;
    }

    public int GetMaxWeight()
    {
        return maxWeight + additionalWeight;
    }

    public float GetCurrentWeight()
    {
        return currentWeight;
    }

    public List<ItemScript> GetInventoryItems()
    {
        return inventoryItems;
    }

    public void SetBackPackLevel(int level)
    {
        additionalWeight = level * additionalMaxWeightPerLevel;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("ResourceBox"))
            inventoryOptions.gameObject.SetActive(false);
    }

    public bool SearchTypeItem(ItemsData.Item type)
    {
        foreach (var item in inventoryItems)
            if (item.GetData() && item.GetData().item == type)
                return true;
        return false;
    }

    public ItemScript SearchNote(ItemsData data)
    {
        foreach (var item in inventoryItems)
        {
            if (item.GetData() && item.GetData().item == ItemsData.Item.Note)
            {
                NoteItemData noteData = (NoteItemData)item.GetData();
                if (noteData && noteData == (NoteItemData)data)
                {
                    return item;
                }
            }
        }
        return null;
    }

}
