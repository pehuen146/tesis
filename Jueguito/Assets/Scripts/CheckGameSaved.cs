﻿using UnityEngine;
using UnityEngine.Events;

public class CheckGameSaved : MonoBehaviour {

    [SerializeField] ToggleEvent FileExists;

    private void Start()
    {
        bool saved = SaveManager.singleton.CheckIfFileExist();

        FileExists.Invoke(saved);
    }
}
