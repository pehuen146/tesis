﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM {

    int[,] fsm;
    int state = 0;

    public FSM(int statesCount, int eventsCount)
    {
        fsm = new int[statesCount, eventsCount];

        for (int i = 0; i < statesCount; i++)
        {
            for (int j = 0; j < eventsCount; j++)
            {
                fsm[i, j] = -1;
            }

        }
    }

    public void SetRelation(State srcState, int evt, State dstState)
    {
        fsm[(int)srcState, evt] = (int)dstState;
    }

    public State GetState()
    {
        return (State)state;
    }

    public void SetState(State _state)
    {
        state = (int)_state;
    }

    public void SetEvent(int evt)
    {
        if(fsm[state, evt] != -1)
            state = fsm[state, evt];
    }
}
