﻿using UnityEngine;
using UnityEngine.UI;

public class WeightText : MonoBehaviour {

    [SerializeField] Text text;
    [SerializeField] InventoryScript inventory;

    private void Update()
    {
        text.text = "Peso: " + inventory.GetCurrentWeight().ToString() + " / " + inventory.GetMaxWeight().ToString();
    }

}
