﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodParticleManager : MonoBehaviour {

    [SerializeField] float timeLapse;

    private void OnEnable()
    {
        Invoke("Disable", timeLapse);
    }

    private void Disable()
    {
        gameObject.SetActive(false);
    }
}
