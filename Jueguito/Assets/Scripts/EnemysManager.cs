﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemysManager : MonoBehaviour
{
    [SerializeField] List<GameObject> enemys;

    public static EnemysManager singleton;

    void Awake()
    {
        if (singleton != null)
        {
            Destroy(gameObject);
        }
        else
            singleton = this;
    }


    public List<GameObject> GetCloseEnemys(float minimumDistance, Vector3 playerPos)
    {
        List<GameObject> closeEnemys = new List<GameObject>();

        foreach (var enemy in enemys)
            if (Vector3.Distance(enemy.transform.position, playerPos) < minimumDistance && enemy.activeInHierarchy)
                closeEnemys.Add(enemy);

        return closeEnemys;
    }
}
