﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundManager : MonoBehaviour
{

    [SerializeField] float soundRange;
    [SerializeField] AudioClip chaseMusic;
    [SerializeField] AudioClip moveObjectSound;

    [SerializeField] AudioClip[] footsteps;
    [SerializeField] AudioSource footstepsAudioSource;

    AudioSource audioSource;
    bool isPlaying = false;
    int enemyChaseState = 0;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySound(AudioClip sound)
    {
        audioSource.PlayOneShot(sound);
    }

    public void PlayGunShotSound(AudioClip[] gunShotSounds)
    {
        if (gunShotSounds.Length > 0)
            audioSource.PlayOneShot(gunShotSounds[Random.Range(0, gunShotSounds.Length)]);

        if (EnemysManager.singleton)
        {
            List<GameObject> enemys = EnemysManager.singleton.GetCloseEnemys(soundRange, transform.position);

            foreach (var enemy in enemys)
            {
                EnemyIA eia = enemy.GetComponent<EnemyIA>();
                if (eia)
                    eia.HearSound(transform.position);
            }
        }
    }

    public void PlayChaseMusic()
    {
        if (!isPlaying)
        {
            audioSource.PlayOneShot(chaseMusic);
            isPlaying = true;
        }
        enemyChaseState++;
    }

    public void Stop()
    {
        enemyChaseState--;
        if (isPlaying && enemyChaseState <= 0)
        {
            audioSource.Stop();
            isPlaying = false;
        }
    }

    public void FootStep()
    {
        if (footsteps.Length > 0 && !footstepsAudioSource.isPlaying)
            footstepsAudioSource.PlayOneShot(footsteps[Random.Range(0, footsteps.Length)]);
    }

    public void PlayObjectMove()
    {
        PlaySound(moveObjectSound);
    }

    public void ResetChase()
    {
        isPlaying = false;
        enemyChaseState = 0;
        audioSource.Stop();
    }
}
