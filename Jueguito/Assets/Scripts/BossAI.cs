﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class BossAI : MonoBehaviour
{
    [SerializeField] EnemyDT dt;
    [SerializeField] UnityEvent Death;
    [SerializeField] float stunnedTime;
    [SerializeField] Transform[] objectives;

    NavMeshAgent agent;
    FSM fsm;
    PatrolPoint patrolPoint; // Current patrol point
    Timer waitTimer;
    Timer attackTimer;
    Animator animator;
    EnemyBossHealth health;
    EnemySoundManager soundManager;
    EnemyDamage enemyDamage;
    bool isChase = false;
    Transform objective;
    Cannon_Script cannon;

    private Vector3 previousPosition;
    public float currentSpeed;

    Vector3 diff;

    void Awake()
    {
        waitTimer = new Timer();
        attackTimer = new Timer();

        attackTimer.SetTime(dt.GetAttackTime(), false);
        waitTimer.SetTime(dt.GetWaitTime(), true);

        agent = GetComponent<NavMeshAgent>();
        health = GetComponent<EnemyBossHealth>();
        enemyDamage = GetComponentInChildren<EnemyDamage>();
    }

    void Start()
    {
        objective = FindObjective();

        if (objective == null)
            objective = PlayerSingleton.singleton.GetPlayer().transform;

        waitTimer.SetActive(false);
        attackTimer.SetActive(true);

        if (PatrolManager.singleton)
            patrolPoint = PatrolManager.singleton.GetAndClosePoint();

        animator = GetComponentInChildren<Animator>();
        soundManager = GetComponent<EnemySoundManager>();

        fsm = new FSM(7, 3);

        fsm.SetRelation(State.Idle, 0, State.Chase);
        fsm.SetRelation(State.Chase, 0, State.Attack);
        fsm.SetRelation(State.Chase, 1, State.NextPatrolPoint);
        fsm.SetRelation(State.Stunned, 0, State.Idle);
        fsm.SetRelation(State.NextPatrolPoint, 0, State.Chase);
        fsm.SetRelation(State.NextPatrolPoint, 1, State.InPatrolPoint);
        fsm.SetRelation(State.InPatrolPoint, 0, State.Chase);
        fsm.SetRelation(State.InPatrolPoint, 1, State.NextPatrolPoint);
        fsm.SetRelation(State.InPatrolPoint, 2, State.Idle);
        fsm.SetRelation(State.Attack, 0, State.Idle);
        fsm.SetRelation(State.Attack, 1, State.Chase);
        fsm.SetRelation(State.HeardSomething, 0, State.Idle);

        fsm.SetState(State.NextPatrolPoint);

        agent.speed = dt.GetSpeed();
    }

    void Update()
    {
        if (Time.timeScale <= 0)
            return;

        UpdateMovementAnimation();

        waitTimer.Update();
        attackTimer.Update();

        if (objective)
            cannon = objective.gameObject.GetComponent<Cannon_Script>();

        if (cannon && !cannon.GetIsAlive())
        {
            DeleteObjective();
            objective = FindObjective();
        }

        if (!objective)
            objective = PlayerSingleton.singleton.GetPlayer().transform;

        if (health.IsAlive())
        {
            switch (fsm.GetState())
            {
                case State.Idle:
                    Idle();
                    break;

                case State.Chase:
                    Chase();
                    break;

                case State.InPatrolPoint:
                    InPatrolPoint();
                    break;

                case State.NextPatrolPoint:
                    NextPatrolPoint();
                    break;

                case State.Attack:
                    Attack();
                    break;

                case State.HeardSomething:
                    HeardSomething();
                    break;

                case State.Stunned:
                    Stunned();
                    break;
            }
        }
        else
        {
            IsStopped(true);
            if (isChase)
            {
                isChase = false;
                PlayerSingleton.singleton.StopChaseMusic();
            }
            Death.Invoke();
        }
    }

    void StunnedToIdle()
    {
        fsm.SetState(State.Idle);
    }

    public void Stun()
    {
        fsm.SetState(State.Stunned);

        Invoke("StunnedToIdle", stunnedTime);

        animator.SetTrigger("Hit");
    }

    void IsStopped(bool value)
    {
        agent.isStopped = value;
    }

    void Attack()
    {
        if (!health.IsAlive() || !objective)
            return;

        agent.SetDestination(objective.position);
        diff = objective.position - transform.position;

        if (diff.magnitude <= dt.GetAttackDistance())
        {
            if (attackTimer.TimeUp())
            {
                //enemyDamage.gameObject.SetActive(true);
                animator.SetTrigger("Attack");
                soundManager.PlayAttackSound();
                attackTimer.Reset();
            }
        }
        else
            fsm.SetEvent(1);
    }

    void Idle()
    {
        IsStopped(true);

        soundManager.PlayCanSeePlayerSound();
        fsm.SetEvent(0);
    }

    void Chase()
    {
        IsStopped(false);

        if (!isChase)
        {
            isChase = true;
        }

        diff = objective.position - transform.position;

        agent.SetDestination(objective.position);
        if (diff.magnitude < dt.GetAttackDistance())
            fsm.SetEvent(0);
    }

    void InPatrolPoint()
    {
        soundManager.PlayCanSeePlayerSound();
        fsm.SetEvent(0);
        waitTimer.Reset();
        waitTimer.SetActive(false);
    }

    void NextPatrolPoint()
    {
        IsStopped(false);
        soundManager.PlayCanSeePlayerSound();
        fsm.SetEvent(0);
    }

    void HeardSomething()
    {
        fsm.SetEvent(0);

        IsStopped(false);
    }

    void Stunned()
    {
        IsStopped(true);
    }

    void UpdateMovementAnimation()
    {
        float previousSpeed = currentSpeed;

        Vector3 curMove = transform.position - previousPosition;
        currentSpeed = curMove.magnitude / Time.deltaTime;
        previousPosition = transform.position;

        currentSpeed = Mathf.Lerp(previousSpeed, currentSpeed, 0.1f);

        animator.SetFloat("Speed", currentSpeed);
    }

    Transform FindObjective()
    {
        Transform currentObjective = null;

        foreach (var item in objectives)
            if (item)
                currentObjective = item;

        return currentObjective;
    }

    void DeleteObjective()
    {
        for (int i = 0; i < objectives.Length; i++)
        {
            if (objective == objectives[i])
                objectives[i] = null;
        }
    }

}
