﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryOptions : MonoBehaviour {

    ItemScript currentItem;

    public void SetCurrentItem(ItemScript item)
    {
        currentItem = item;
    }

	public void Move()
    {
        if (currentItem)
            currentItem.Move();

        ResetOptions();
    }

    public void Use()
    {
        if (currentItem)
            currentItem.Use();

        ResetOptions();
    }

    public void ResetOptions()
    {
        gameObject.SetActive(false);
        currentItem = null;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("ResourceBox"))
            gameObject.SetActive(false);
    }
}
