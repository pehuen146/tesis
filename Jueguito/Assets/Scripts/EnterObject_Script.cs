﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterObject_Script : MonoBehaviour {

    [SerializeField] string enterCarTag;
    [SerializeField] string CanonTag;

    GameObject cameraTransform;
    Cannon_Script canon;
    CarInput ci;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(enterCarTag))
            ci = other.GetComponentInParent<CarInput>();

        if (other.CompareTag(CanonTag))
            canon = other.GetComponentInParent<Cannon_Script>();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(enterCarTag))
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                cameraTransform = GetComponentInChildren<GetGameobject>().GetGameObject();

                cameraTransform.transform.position = ci.transform.position;

                ci.SetCameraTransform(cameraTransform);
                cameraTransform.transform.parent = ci.gameObject.transform;
                ci.SetPlayer(gameObject);
                gameObject.SetActive(false);
            }
        }

        if (other.CompareTag(CanonTag))
            if (Input.GetKeyDown(KeyCode.E))
                canon.EnterCanon(gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        ci = null;
    }

    public CarInput GetCarInput()
    {
        return ci;
    }

}
