using UnityEngine.UI;
using UnityEngine;

public class Health : MonoBehaviour
{

    [SerializeField] float maxHealth;
    [SerializeField] HealthBarScript healthBar;

    float health;

    void Start()
    {
        if (SaveManager.singleton.GetGameContinued())
            return;

        health = maxHealth;
        healthBar.UpdateHealthBar(health, maxHealth);
    }

    public float GetHealth()
    {
        return health;
    }

    public void SetHealth(float value)
    {
        health = value;

        if (health > maxHealth)
            health = maxHealth;

        healthBar.UpdateHealthBar(health, maxHealth);
    }

    public void TakeDamage(float damage)
    {
        if ((health - damage) > 0)
        {
            health -= damage;
            healthBar.UpdateHealthBar(health, maxHealth);
        }
        else
        {
            health = 0;
            healthBar.UpdateHealthBar(health, maxHealth);
            Death();
        }
    }

    public void Heal(float healAmount)
    {
        health += healAmount;
        if (health > maxHealth)
            health = maxHealth;
        healthBar.UpdateHealthBar(health, maxHealth);
    }

    void Death()
    {
	   SceneManagement.singleton.LoadScene("Perdido");
        //SceneManagement.singleton.LoadMenu();
    }
}
