﻿using UnityEngine.SceneManagement;
using UnityEngine;
using System.IO;

public class SceneManagement : MonoBehaviour {
    public static SceneManagement singleton;
    
    [SerializeField] SceneField menuScene;
    [SerializeField] SceneField gameScene;
    [SerializeField] SceneField doorOpening;

    string nextScene;

    uint doorIndex;
    
    void Awake()
    {
        DontDestroyOnLoad(this);
        if (singleton != null)
            Destroy(gameObject);
        else
            singleton = this;
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene(menuScene);
        Time.timeScale = 1;
        Destroy(SaveManager.singleton.gameObject);
        Destroy(PlayerSingleton.singleton.gameObject);
    }

    public void LoadGame(bool newGame)
    {
        if (newGame)
            SceneManager.LoadScene(gameScene);
        else
        {
            string savedScene = SaveManager.singleton.GetSavedScene();

            Debug.Log(savedScene);

            SceneManager.LoadScene(savedScene);
        }
    }

    public void LoadScene(string sceneName, uint _doorIndex)
    {
        nextScene = sceneName;

        doorIndex = _doorIndex;

        SaveManager.singleton.Save();

        SceneManager.LoadScene(doorOpening);
    }

    public void NextScene()
    {
        SceneManager.LoadScene(nextScene);
    }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public uint GetDoorIndex()
    {
        return doorIndex;
    }
}
