﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashlightActivator : MonoBehaviour
{
    [SerializeField] GameObject hatlight;
    [SerializeField] GameObject flashlight;
    [SerializeField] new GameObject light;

    private void Awake()
    {
        hatlight.SetActive(false);
        flashlight.SetActive(false);
        light.SetActive(false);
    }

    public void ActivateFlashlight()
    {
        hatlight.SetActive(true);
        flashlight.SetActive(true);
        light.SetActive(true);
    }

    public bool isFlashlightActivated()
    {
        return light.activeInHierarchy;
    }

}
