﻿using UnityEngine.UI;
using UnityEngine;

public class BulletsUI : MonoBehaviour {

    [SerializeField] Text bulletsTxt;

    int currentAmmo = 0;
    int maxAmmo = 0;

    private void Update()
    {
        bulletsTxt.text = currentAmmo + " / " + maxAmmo;
    }

    public void SetMaxAmmo(int _maxAmmo)
    {
        maxAmmo = _maxAmmo;
    }

    public void SetCurrentAmmo(int _currentAmmo)
    {
        currentAmmo = _currentAmmo;
    }

}
